# Markdown Activity

Time: 15m

## Roles

- Manager:
- Recorder:
- Spokesperson:

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown?
Markdown is a lightweight markup language that you can use to add formatting elements to plaintext text documents.
2. What is CommonMark?
CommonMark is a plain text format for writing structured documents based on formatting conventions from email.
3. What is GitHub Flavored Markdown?
GitHub flavored markdown is a dialect of Markdown that is currently supported for user content on GitHub.com and GitHub Enterprise, two services created by GitHub.
4. What kind of file do you use to create Markdown?
You use a .md file.
5. In Markdown How do you create
    1. A heading?
    # Heading
    2. A list?
    - A hyphen, + a space.
    3. A sub-list?
    Like a list, with indentation.
    4. An ordered list?
    you use \<ol>\</ol>, and define the items with \<li>\</li>
    5. A pre-formatted block?
    ```
    Three ticks before and after
    ```
    6. Make something bold?
    Surrounding double **astericks**
    7. Make something italic?
    Surrounding single *astericks*
    8. Make something both bold and italic?
    Surrounding triple ***astericks*** 
    9. Link to something?
    [Word in brackets, link in parenthesis](xkcd.com)
    10. Add a horizontal rule (bar or separator)?
    --- 
    Triple hyphen
6. Besides VSCode, how can you view a rendered Markdown file?
You can use GitHub, GitLab, obsidian, notion, and countless other editors, there are also web frameworks that generate static sites with markdown.
(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.
