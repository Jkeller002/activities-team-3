# Agile

## Roles

* Manager: CT
* Recorder: AF
* Spokesperson: CF
* Researcher: CF


## Model 1 - What is Agile?

12m Video: *What is Agile?* by Mark Shead. May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

Questions: 5m

1. Is Agile a process?
No

2. Is Agile a framework?
No

3. Is Agile a set of tools?
No

4. What is Agile?
Agile is a set of values and principles

5. What does Agile help companies and developers do?
Agile is a collection of beliefs that teams can use to help make decisions on how to develop their software.

Class discussion: 5m


## Model 2 - The Agile Manifesto

Time: 10m

At the core of The Agile Manifesto are four value statements.
They are structured as follows:

```
We value

    Individuals  and Interaction  over  process  and tools

    Working  software  over  comprehensive  documentation

    customer  collaboration  over  contract  negotation

    responding  to change  over  following  a plan

```
The missing phrases are mixed up below. Unscramble them to reconstruct the
value statements of the Agile Manifesto. Try to do it without looking up the Manifesto.

* `Individuals`
* `Working`
* `Responding`
* `Customer`
* `documentation`
* `and tools`
* `negotiation`
* `collaboration`
* `following`
* `and interactions`
* `software`
* `comprehensive`
* `proccess`
* `to change`
* `a plan`
* `contract`

Once you are satisfied, check your work against [The Agile Manifesto](https://www.agilealliance.org/agile101/the-agile-manifesto/) . How many statements did your team get right (ignoring the order of the statements)?

1. Does the Manifesto imply that projects should not use tools or follow a process? No
2. Does the Manifesto imply that projects should not write any documentation? No
3. Does the Manifesto imply that projects should not have contracts with clients? No
4. Does the Manifesto imply that projects should not create or follow a plan? No
5. Based on their values, why would an Agile team reject the waterfall method to developing software? 
   They would reject the Waterfall method, as it has the opposite values of that of the Agile method.

Class discussion: 10m


## Model 3

Time: 20m

Below are the 12 Agile principles that embody the 4 Agile value statements.
For each, identify which value statement the principle supports or is most related to,
and be prepared to explain how.

1. Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
- Responding to change over following a plan

2. Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
- Responding to change over following a plan

3. Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
- Working  software  over  comprehensive  documentation

4. Business people and developers must work together daily throughout the project.
- customer  collaboration  over  contract  negotation

5. Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
- Individuals  and Interaction  over  process  and tools

6. The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
- Individuals  and Interaction  over  process  and tools

7. Working software is the primary measure of progress.
- Working  software  over  comprehensive  documentation

8. Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
- customer  collaboration  over  contract  negotation

9. Continuous attention to technical excellence and good design enhances agility.
- Responding to change over following a plan

10. Simplicity--the art of maximizing the amount of work not done--is essential.
- Working  software  over  comprehensive  documentation

11. The best architectures, requirements, and designs emerge from self-organizing teams.
- Individuals  and Interaction  over  process  and tools

12. At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.
- Individuals  and Interaction  over  process  and tools

Class discussion: 20m


## References

* 12m Video: *What is Agile?* by Mark Sheed May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

* The Agile Manifesto

  * Values

    * Original: <https://agilemanifesto.org/>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/the-agile-manifesto/>

  * Principles

    * Original: <https://agilemanifesto.org/principles.html>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/>


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
