# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter: Cam
* Recorder & Pilot: Connor
* Quality Control: Aaron
* Efficiency Expert: Cam

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea?
No. You can't copyright an idea.
2. You have written a new program based on that idea, is that software copyright-able?
Yes, it's your program.
3. You have written a new program, what do you have to do to copyright that program?
It's automatic.
4. If you own the copyrights to something, what can you do that no one else can do? (see <http://www.bitlaw.com/copyright/scope.html> )

- The right to reproduce the copyrighted work.
- The right to prepare derivative works based upon the work.
- The right to distribute copies of the work to the public.
- The right to perform the copyrighted work publicly.
- The right to display the copyrighted work publicly.

5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?
You can't do anything, because by default you have no rights to it.
6. What is the purpose of a license?
To define what people can and cannot do with a piece of software.
7. When you license your program do you give up your copyright?
No.
8. As the copyright holder, once you license your program, could you later license it under a different license?
Yes.

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license?
The versions that were distributed under the old license can be used under the previous license.
10. Can you sell open-source licensed software?
Yes, for as much as you want.
11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?
The company owns the copyright on all of the code that you write.
12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?
You would own the parts that you wrote. You should include the MIT license in every distribution of the software.
13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?
You must track the changes and modifications made to the software. The code that they write is under a copyright that is owned by them, and is under the GPL-v3.0

14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core> Mozilla Public License, version 2.0 
    2. <https://github.com/apache/fineract> Apache License Version 2.0
    3. Find VSCodium on GitHub. MIT License

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.

Mozilla Public License 2.0 

Cans - Commercial Use, Modification, Distribution, Sublicensing, Place Warranty, Use Patent Claims
Cannots - Use Trademark, Hold Liable
Musts - Include Copyright, Include License, Disclose Source, Include Original

Apache License Version 2.0

Cans - Commercial Use, Modification, Distribution, Sublicensing, Place Warranty, Private Use, Patent Claims
Cannots - Hold Liable, Hold Trademark
Musts - Include Copyright, Include License, State Changes, Include Notice

MIT License

Cans - Commercial Use, Modification, Distribution, Sublicensing, Private Use
Cannots - Hold Liable
Musts - Include Copyright, Include License

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?

Copyleft licenses control more how "open" any changes to the code must be, and how redistributors can use the code.

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
