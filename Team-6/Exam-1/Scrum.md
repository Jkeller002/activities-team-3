# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: CF
* Recorder: CT
* Spokesperson: AF
* Researcher: AF


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition, Theory, and Values

Time: 25m

Read the section `Scrum Definition` and answer the following questions.

1. True or false; you can apply Scrum as described in The Scrum Guide.
- True
2. True or false; you should not deviate from Scrum as described in The Scrum Guide.
- False
3. True or false; Scrum is a completely defined process for software development.
- False

Read the section `Scrum Theory` and answer the following questions.

1. What are the three pillars of Scrum?
- Transparency, Inspection, and Adaptation.
2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?
- It would make the other pillars unuseable. Without transparency, inspection cannot occur.
3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?
- You cannot do Adaptation without Inspection but it does not affect transparency.
4. What is necessary for "adaptation" to be possible?
- The people involved have to be self managing and self sufficient while also using the other pillars.

Read the section `Scrum Values` and answer the following questions.

1. What values does Scrum add to the Agile values?
- Scrum adds a focus on commitment, focus, openness, respect, and courage.
2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.
- It's read

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 4: Scrum Team

Time: 20m

Read the section `Scrum Team` and answer the following questions.


1. What roles are there in the Scrum Team?
- Scrum Master
- Product Owner
- Developers
2. How big should a Scrum Team be?
- 10 or fewer people.
3. Which of these teams is cross-functional?
   1. A database team that is able to design and implement the database. <<< This one
   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert. 
4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog. Developers.
   2. Clearly communicates items in the Product Backlog. Product Owner.
   3. Ensures Scrum events take place and are positive, productive, and time-boxed. Scrum Master
   4. Orders/prioritizes the items in the Product Backlog. Product Owner.
   5. Removes barriers between stakeholders and the Scrum Team. Scrum Master.
   6. Holds the team accountable as professionals. Developers.
   7. Adhering to the Definition of Done. All.
   8. Adapting the plan each day toward the Sprint Goal. Developers.
   9. Clearly communicates the Product Goal. Product Owner.

---

> ***STOP*** Review as a class before continuing.

Time: 10m

---

## Model 5: Scrum Events

Read the section `Scrum Events` and answer the following questions.

Time: 30m

Scrum Events

1. What are the major Scrum events?

- Sprint
- Planning
- Daily
- Review
- Retro

2. What is the fundamental purpose of all Scrum events?

Formal opportunities to inspect and adapt scrum artifacts.

Sprints

3. How long is a typical Sprint?
1 month or less.
4. True or false; the length of a Sprint is determined by the amount of work that needs to be done? False
5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint? False
6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent the Team from meeting the Spring Goal on time. Which of the following is allowed by Scrum?
   1. Renegotiate the end of the current Sprint? No
   2. Renegotiate the quality of the work being done in the current Sprint? No
   3. Renegotiate the scope of the work of the current Sprint? Yes
7. When may a Sprint be canceled and by whom?
The Product Owner can cancel a sprint when the sprint goal becomes obselete.

Sprint Planning

1. Purpose? Lays out the work to be performed during the sprint.
2. Length? A maximum of 8 hours for a one month sprint, and less for shorter sprints.
3. Frequency? Once per sprint.
4. When during the Sprint? Before the sprint starts.
5. Who are involved? The whole team.
6. What is the input?  Ways to make the product better.
7. What is the output? Plan for the sprint.

Daily Scrum

1. Purpose? Inspect progress to the sprint goal, and adapt the sprint backlog.
2. Length? 15 minutes.
3. Frequency? Daily.
4. When during the Sprint? Every day during the sprint daily.
5. Who are involved? Developers, optionally Scrum Master and Product Owner.
6. What is the input? Technique and structure they want to use.
7. What is the output? Actionable plan for the next day of work.

Sprint Review

1. Purpose? Inspect the outcome of the Sprint and determine future adaptations.
2. Length? 4 Hours or less.
3. Frequency? Once per Sprint.
4. When during the Sprint? At the end.
5. Who are involved? All members of the scrum team.
6. What is the input? Accomplishments of the prior Sprint. 
7. What is the output? Adaptations for the next sprint

Sprint Retrospective

1. Purpose? Plan ways to increase quality and effectiveness.
2. Length? 3 hours or less.
3. Frequency? once.
4. When during the Sprint? After the review.
5. Who are involved? All members of the scrum team.
6. What is the input? Discussion of how the sprint went.
7. What is the output? Helpful changes for the next sprint.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 6: Scrum Artifacts

Time: 30m

Read the section `Scrum Artifacts` and its subsections and answer the following questions.

Product Backlog

1. What is it and what does it contain?
2. Who is responsible for sizing an item in the backlog?
3. Who is responsible for ordering the items in the backlog (highest priority first)?
4. What is the Product Goal and its purpose?

Sprint Backlog

1. What is in the Sprint Backlog?
2. What is the Sprint Goal and its purpose?

Increment
1. What is it?
2. How many may be produced as part of a sprint?
3. What is the Definition of Done?
4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
