# Markdown Activity

Time: 15m

## Roles

- Manager: Jake M   
- Recorder: Taylor M
- Spokesperson: Justin B

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown?
    "Markdown is a lightweight markup language that you can use to add formatting elements to plaintext text documents."
2. What is CommonMark?
    "CommonMark is a rationalized/standardized version of Markdown syntax with a spec whose goal is to remove the ambiguities and inconsistency surrounding the original Markdown specification."
3. What is GitHub Flavored Markdown?
    "GitHub Flavored Markdown, often shortened as GFM, is the dialect of Markdown that is currently supported for user content on GitHub.com and GitHub Enterprise.
    This formal specification, based on the CommonMark Spec, defines the syntax and semantics of this dialect.
    GFM is a strict superset of CommonMark"
4. What kind of file do you use to create Markdown?
    "The file should have an .md or .markdown extension."
5. In Markdown How do you create
    1. A heading?
        "Add number signs (#) in front of a word or phrase. The number of number signs you use should correspond to the heading level. For example, to create a heading level three (<h3>), use three number signs (e.g., ### My Header)."
    2. A list?
        "You can use -, + or * at the very beginning of the line to create an unordered list.
        You can use a digit followed by a fullstop, eg. 1. to create a numbered list
        You can create a list with tickboxes by using - [ ]"
    3. A sub-list?
        "To nest one list within another, indent each item in the sublist by four spaces. You can also nest other elements like paragraphs, blockquotes or code blocks."
    4. An ordered list?
        "To create an ordered list, add line items with numbers followed by periods. The numbers don’t have to be in numerical order, but the list should start with the number one." (1. /n 1. /n 1. /n)
    5. A pre-formatted block?
        "Using indentation
        Using one or more backticks at the beginning and the end of a preformatted section" (```Python /n code /n ```)
    6. Make something bold?
        "To make a phrase bold, you add two asterisks before and after it"
    7. Make something italic?
        "To italicize text, add one asterisk or underscore before and after a word or phrase. To italicize the middle of a word for emphasis, add one asterisk without spaces around the letters."
    8. Make something both bold and italic?
        "To emphasize text with bold and italics at the same time, add three asterisks or underscores before and after a word or phrase. To bold and italicize the middle of a word for emphasis, add three asterisks without spaces around the letters."
    9. Link to something?
        "To create a link, enclose the link text in brackets (e.g., [Duck Duck Go]) and then follow it immediately with the URL in parentheses (e.g., (https://duckduckgo.com%29/)."
    10. Add a horizontal rule (bar or separator)?
        "To create a horizontal rule, use three or more asterisks (***), dashes (---), or underscores "
6. Besides VSCode, how can you view a rendered Markdown file?
    "There are online markdown editors such as StackEdit and there are desktop markdown editors. You can also find browser extensions to do the job."

(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.
