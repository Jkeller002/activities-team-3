# Agile

## Roles

* Manager: JM   
* Recorder: TM
* Spokesperson: JM, JB
* Researcher: JM, JB


## Model 1 - What is Agile?

12m Video: *What is Agile?* by Mark Shead. May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

Questions: 5m

1. Is Agile a process?
    No.

2. Is Agile a framework?
    No.

3. Is Agile a set of tools?
    No.

4. What is Agile?
    Agile is a set of values and principles. 

5. What does Agile help companies and developers do?
    It helps them make decions that best fit their circumstances and adapt to change while developing software.

Class discussion: 5m


## Model 2 - The Agile Manifesto

Time: 10m

At the core of The Agile Manifesto are four value statements.
They are structured as follows:

```
We value

    Individuals  and interractions  over  processes and tools

    Customer collaboration  over  contract  negotiation

    Responding to change over following a plan

    Working software over comprehensive documentation

```
The missing phrases are mixed up below. Unscramble them to reconstruct the
value statements of the Agile Manifesto. Try to do it without looking up the Manifesto.

* `Individuals`
* `Working`
* `Responding`
* `Customer`
* `documentation`
* `and tools`
* `negotiation`
* `collaboration`
* `following`
* `and interactions`
* `software`
* `comprehensive`
* `proccess`
* `to change`
* `a plan`
* `contract`

Once you are satisfied, check your work against [The Agile Manifesto](https://www.agilealliance.org/agile101/the-agile-manifesto/) . How many statements did your team get right (ignoring the order of the statements)?
    All of our statements were correct, ignoring the order.

1. Does the Manifesto imply that projects should not use tools or follow a process?
    No, but there are better methods that can be used.
2. Does the Manifesto imply that projects should not write any documentation?
    No, but there are better methods that can be used.
3. Does the Manifesto imply that projects should not have contracts with clients?
    No, but there are better methods that can be used.
4. Does the Manifesto imply that projects should not create or follow a plan?
    No, but there are better methods that can be used.
5. Based on their values, why would an Agile team reject the waterfall method to developing software?
    It is slow to react to change and does not involve as much collaboration.

Class discussion: 10m


## Model 3

Time: 20m

Below are the 12 Agile principles that embody the 4 Agile value statements.
For each, identify which value statement the principle supports or is most related to,
and be prepared to explain how.

1. Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
    "Working software over comprehensive documentation". Frequent and continous delivery of software means that the team can adapt and change requirements.

2. Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
    "Responding to change over following a plan." Reacting to changing specs keeps the project flowing smoothly.

3. Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
    "Working software over comprehensive documentation". This allows frequent testing and bug squishing as well as feedback so less time is spent on features that go unused.

4. Business people and developers must work together daily throughout the project.
    "Individuals and interractions over processes and tools". Constant communication can keep a project on track

5. Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
    "Individuals and interractions over processes and tools." Investing in individuals and making a team that works well together will go further to make projects work.

6. The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
    "Individuals and interractions over processes and tools." Conveying messages face-to-face helps a team work well together to make the process of completing a project easier.

7. Working software is the primary measure of progress.
    "Working software over comprehensive documentation." Building a project piece by piece allows for easier bug fixing and keeps the project in the desired scope.

8. Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
    "Customer collaboration over contract negotiation." Keeping in constant contact with all parties will allow for constant updates and progress until the product is finished.

9. Continuous attention to technical excellence and good design enhances agility.
    "Working software over comprehensive documentation." If the software works well and is bug free it is easier to implement the next segment of code.

10. Simplicity--the art of maximizing the amount of work not done--is essential.
    "Working software over comprehensive documentation." Working on smaller functional code helps keep the project on track and away from tangents.

11. The best architectures, requirements, and designs emerge from self-organizing teams.
    "Individuals and interractions over processes and tools." If a team works well together, they are more likely to develop a successful project.

12. At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.
    "Responding to change over following a plan." When a team encounters a roadblock, they should be able to adapt and improve to better handle the project.

Class discussion: 20m


## References

* 12m Video: *What is Agile?* by Mark Sheed May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

* The Agile Manifesto

  * Values

    * Original: <https://agilemanifesto.org/>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/the-agile-manifesto/>

  * Principles

    * Original: <https://agilemanifesto.org/principles.html>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/>


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
