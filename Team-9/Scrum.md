# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: TM
* Recorder: JM
* Spokesperson: JM
* Researcher: TM


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition, Theory, and Values

Time: 25m

Read the section `Scrum Definition` and answer the following questions.

1. True or false; you can apply Scrum as described in The Scrum Guide. 
   TRUE
2. True or false; you should not deviate from Scrum as described in The Scrum Guide.
   FALSE
3. True or false; Scrum is a completely defined process for software development.
   FALSE

Read the section `Scrum Theory` and answer the following questions.

1. What are the three pillars of Scrum?
   transparency, inspection, and adaptation.
2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?
   It can lead to decisions that diminish value and increase risk... Inspection without transparency is misleading and wasteful.
3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?
   Without inspection, you can't adapt because you can't find desireable problems and transparency enables inspection.  
4. What is necessary for "adaptation" to be possible?
   Effective transparency and inspection enable adaptation to occur which persuades a Scrum Team to adapt. 
Read the section `Scrum Values` and answer the following questions.

1. What values does Scrum add to the Agile values?
   Provides the framework for adapting to change
   The courage value of Scrum teams add to Agile values as it focuses on the courage to do the right thing and to work on tougher problems as a team. 
   Basically, empowerment within the team. 
   
2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 4: Scrum Team

Time: 20m

Read the section `Scrum Team` and answer the following questions.


1. What roles are there in the Scrum Team?
   (1) Scrum master, (1)Product owner, (many)developers 
2. How big should a Scrum Team be?
   10 or fewer people
3. Which of these teams is cross-functional?
   1. A database team that is able to design and implement the database.
   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert.
   Team 2 because everyone has a specialization that will allow them to work together effectively 
4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog.
      developers
   2. Clearly communicates items in the Product Backlog.
      product owner
   3. Ensures Scrum events take place and are positive, productive, and time-boxed.
      Scrum Master
   4. Orders/prioritizes the items in the Product Backlog.
      product owner
   5. Removes barriers between stakeholders and the Scrum Team.
      Scrum Master
   6. Holds the team accountable as professionals.
      developers
   7. Adhering to the Definition of Done.
      developers
   8. Adapting the plan each day toward the Sprint Goal.
      developers
   9. Clearly communicates the Product Goal.
      product owner 

---

> ***STOP*** Review as a class before continuing.

Time: 10m

---

## Model 5: Scrum Events

Read the section `Scrum Events` and answer the following questions.

Time: 30m

Scrum Events

1. What are the major Scrum events?
   The Sprint, Sprint planning, Daily Scrum, Sprint Review, and Sprint Retrospective
2. What is the fundamental purpose of all Scrum events?
   Each event in Scrum is a formal opportunity to inspect and adapt Scrum artifacts. It enables transparency. 

Sprints

3. How long is a typical Sprint?
   1 month or less
4. True or false; the length of a Sprint is determined by the amount of work that needs to be done?
   True 
5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint?
   False
6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent the Team from meeting the Spring Goal on time. Which of the following is allowed by Scrum?
   1. Renegotiate the end of the current Sprint?
   2. Renegotiate the quality of the work being done in the current Sprint?
   3. Renegotiate the scope of the work of the current Sprint?

   allowed: 2 and 3 

7. When may a Sprint be canceled and by whom?
   A Sprint could be cancelled if the Sprint Goal becomes obsolete. Only the Product Owner has the authority to cancel the Sprint.

Sprint Planning

1. Purpose?
   Sprint Planning initiates the Sprint by laying out the work to be performed for the Sprint. This resulting plan is created by the collaborative work of the entire Scrum Team.
2. Length?
   Sprint Planning is timeboxed to a maximum of eight hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.
3. Frequency?
   once
4. When during the Sprint?
   beginning of the Sprint
5. Who are involved?
   ALL - Scrum Master, product owner, and developers
6. What is the input?
   items from the Product Backlog to include in the Sprint
7. What is the output?
   The Sprint Backlog

Daily Scrum

1. Purpose?
   The purpose of the Daily Scrum is to inspect progress toward the Sprint Goal and adapt the Sprint Backlog as necessary, adjusting the upcoming planned work.
2. Length?
   The Daily Scrum is a 15-minute event for the Developers of the Scrum Team. To reduce complexity, it is held at the same time and place every working day of the Sprint.
3. Frequency?
   daily
4. When during the Sprint?
   everyday during the length of the Sprint
5. Who are involved?
   developers
6. What is the input?
   inspection of progress toward Sprint Goal
7. What is the output?
   an actionable plan for the next day of work

Sprint Review

1. Purpose?
   The purpose of the Sprint Review is to inspect the outcome of the Sprint and determine future adaptations. The Scrum Team presents the results of their work to key stakeholders and progress toward the Product Goal is discussed.
2. Length?
   timeboxed to a maximum of four hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.
3. Frequency?
   once
4. When during the Sprint?
   2nd to last Scrum Event
5. Who are involved?
   ALL - Scrum Team and stakeholders
6. What is the input?
   the outcome of the Sprint
7. What is the output?
   decision on what to do next and adjustment of Product Backlog

Sprint Retrospective

1. Purpose?
   The purpose of the Sprint Retrospective is to plan ways to increase quality and effectiveness.
2. Length?
   It is timeboxed to a maximum of three hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.
3. Frequency?
   once
4. When during the Sprint?
   end of Sprint
5. Who are involved?
   ALL - the Scrum Team... SM and developers 
6. What is the input?
   The Scrum Team discusses what went well during the Sprint, what problems it encountered, and how those problems were (or were not) solved.
7. What is the output?
   changes to improve the outcome/effectiveness of the next Sprint

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 6: Scrum Artifacts

Time: 30m

Read the section `Scrum Artifacts` and its subsections and answer the following questions.

Product Backlog

1. What is it and what does it contain?
   The Product Backlog is an emergent, ordered list of what is needed to improve the product. It is the single source of work undertaken by the Scrum Team.

2. Who is responsible for sizing an item in the backlog?
   developers

3. Who is responsible for ordering the items in the backlog (highest priority first)?
   Product Owner

4. What is the Product Goal and its purpose?
   The Product Goal describes a future state of the product which can serve as a target for the Scrum Team to plan against. The Product Goal is in the Product Backlog. The rest of the Product Backlog emerges to define “what” will fulfill the Product Goal.
   It is the long-term objective for the Scrum Team. They must fulfill (or abandon) one objective before taking on the next.

Sprint Backlog

1. What is in the Sprint Backlog?
   The Sprint Backlog is composed of the Sprint Goal (why), the set of Product Backlog items selected for the Sprint (what), as well as an actionable plan for delivering the Increment (how). 

2. What is the Sprint Goal and its purpose?
   The Sprint Goal is the single objective for the Sprint. The Sprint Goal also creates coherence and focus, encouraging the Scrum Team to work together rather than on separate initiatives.

Increment
1. What is it?
   An Increment is a concrete stepping stone toward the Product Goal. Each Increment is additive to all prior Increments and thoroughly verified, ensuring that all Increments work together. In order to provide value, the Increment must be usable.

2. How many may be produced as part of a sprint?
   as many as needed

3. What is the Definition of Done?
   The Definition of Done is a formal description of the state of the Increment when it meets the quality measures required for the product.
   
4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?
   if a Product Backlog item does not meet the Definition of Done, it cannot be released or even presented at the Sprint Review. Instead, it returns to the Product Backlog for future consideration

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
