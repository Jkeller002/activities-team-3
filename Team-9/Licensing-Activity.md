# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter:TM
* Recorder & Pilot:JB
* Quality Control:JM
* Efficiency Expert:JM, TM, JB

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea?

Copyright is given implicity to the author or creator of an idea once that idea has been expressed in some way.

2. You have written a new program based on that idea, is that software copyright-able?

Now that the program exists, its author is automatically given copyright.

3. You have written a new program, what do you have to do to copyright that program?

Nothing for a copyright, it is considered intrinsic to creation. Filing for a patent on a tangible product allows you to copyright the program.

4. If you own the copyrights to something, what can you do that no one else can do? (see http://www.bitlaw.com/copyright/scope.html )
"1: The right to reproduce the copyrighted work.
2: The right to prepare derivative works based upon the work.
3: The right to distribute copies of the work to the public.
4: The right to perform the copyrighted work publicly.
5: The right to display the copyrighted work publicly."

5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?
You may not do any of the things that the copyright owner can do. Sometimes, you may adapt the software if it meets the standard of fair use.

6. What is the purpose of a license?
A license protects the intellectual property of whoever created a copyrighted material. It provides strict standards for how a product may be used and distributed.

Used to give others the right to use, modify, and share a software. It also attaches conditions such as requiring proper attribution.

7. When you license your program do you give up your copyright?
Copyright is not given up unless specified, when licensing software you do not give up your copyright.

8. As the copyright holder, once you license your program, could you later license it under a different license?
Yes, as a copyright holder you will always be able to release the work under a different license.

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license?
The previous license will still be available for use.

10. Can you sell open-source licensed software?
You are able to sell it, according to the open-source definition all open source software can be used for commercial purposes and are allowed to sell it. 

11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?
Usually the company will own any code you have done, however it depends on how the company is set up to work with developers

12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?
Each contributor can have their own indiviudal copyright or they group can come together to come up with a collaborative copyright license
Under MIT's license it is essential according to the MITS terms and conditionsto include both the copyright notice and permission notice in all copies of the software.

13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?
you must include a copy of the GPL-v.0. licensed texts in your projects and clearly state that your project is licensed under it. Your friend would own any code that they have written, and the license will be under GPL-v3.0

14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core>
    Mozilla Public License, version 2.0
    2. <https://github.com/apache/fineract>
    Apache License, version 2.0
    3. Find VSCodium on GitHub.
    MIT license

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.

APACHE: cans: commercial use, modify, distribute, sublicense, place warranty, private use, use patent claims, 
CANT: hold liable, use trademark
MUST: include copyright, include license, state changes, include notice

Mozilla: cans: commercial use, modify, distribute, sublicense, place warranty, use patent claims, 
CANT: use trademark, hold liable
MUST: include copyright, include license, disclose source, include original

MIT: cans: Commercial use, Modify, Distribute, Sublicense, Private use
CANT: hold liable
MUST: include copyright, include license

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?

Permissive licenses are more flexible, have minimmal restrictions on use, modification, and distribution
Copyleft: requires any modifications to included under the ssame license

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
