# Agile vs Waterfall


## Roles

Assign each role below to a team member. Put your recorder in the middle.

* Manager: NL
* Recorder: EP
* Spokesperson: AL
* Quality Control: AL


## Model 1

Time: 20m

See Sweeney's article "Agile vs Waterfall: Which Method is More Successful" https://clearcode.cc/blog/agile-vs-waterfall-method/

Use the above article to answer the following questions.

1. In `Agile vs Waterfall: Project Success and Failure Rates`...

    a. Results from what studies are reported?

    Ambysoft's 2013 project success rate survey

    b. When were the studies conducted?

    the studies were conducted in 2013

    c. Summarize the results of the studies.

    Agile has a 15% increase of success rate compared to waterfall

    d. What conclusions do you draw from these results?

    Agile is a better method to use


2. In `1. The Software-Development Process`, inspect the diagrams for
   the Agile and Waterfall processes.

    a. How are Agile and Waterfall the same?

    They both create a timeline in which users/developer can add or take away from the development.

    b. How are Agile and Waterfall different?

    agile can go back in developement without issues. agile has a better quality control as everything is tested
    together rather than individualy 


3. In `2. Application Testing` ...

    a. Summarize the differences in testing between Agile and Waterfall.

    Waterfall is tested seperately while agile can be tested together.


4. In `3. Value delivery` ...

    a. Explain the "Boat" diagram.

    b. The Mona Lisa is often used to illustrate incremental and iterative development. Search for these and summarize your understanding of these concepts and how they relate to agile development. How does this modify your understanding of Agile and Waterfall?

5. If you have more time, see if there are any more recent studies with new data comparing Agile and Waterfall. Link to them here.

(Class discussion 15m)


## Other articles on Waterfall vs Agile

*   [https://clearcode.cc/blog/agile-vs-waterfall-method/](https://clearcode.cc/blog/agile-vs-waterfall-method/)
*   [https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/](https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/)
*   [https://www.infoq.com/articles/standish-chaos-2015](https://www.infoq.com/articles/standish-chaos-2015)


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
