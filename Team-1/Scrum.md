# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: AL
* Recorder: EP
* Spokesperson: NL
* Researcher: NL


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition, Theory, and Values

Time: 25m

Read the section `Scrum Definition` and answer the following questions.

1. **True** or false; you can apply Scrum as described in The Scrum Guide.
2. **True** or false; you should not deviate from Scrum as described in The Scrum Guide.
3. True or **false**; Scrum is a completely defined process for software development.

Read the section `Scrum Theory` and answer the following questions.

1. What are the three pillars of Scrum?

The three pillars are transparency, inspection, adaptation

2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?

you couldnt ispected so you couldnt adaptation either

3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?

nothing would get looked over (Errors could occur)

4. What is necessary for "adaptation" to be possible?

The other 2 pillars allow you to be able to adaped

Read the section `Scrum Values` and answer the following questions.

1. What values does Scrum add to the Agile values?

the values for scrum show why we value the left side agile values.

2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 4: Scrum Team

Time: 20m

Read the section `Scrum Team` and answer the following questions.


1. What roles are there in the Scrum Team?

there are scrum master, product owner, and developers

2. How big should a Scrum Team be?

no more than 10

3. Which of these teams is cross-functional?

team 2 is cross-fuctional

   1. A database team that is able to design and implement the database.
   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert.
4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog. **Developers**
   2. Clearly communicates items in the Product Backlog. **product owner**
   3. Ensures Scrum events take place and are positive, productive, and time-boxed. **scrum master**
   4. Orders/prioritizes the items in the Product Backlog. **product owner**
   5. Removes barriers between stakeholders and the Scrum Team. **scrum master**
   6. Holds the team accountable as professionals. **developers**
   7. Adhering to the Definition of Done. **developers**
   8. Adapting the plan each day toward the Sprint Goal. **developers**
   9. Clearly communicates the Product Goal. **product owner**

---

> ***STOP*** Review as a class before continuing.

Time: 10m

---

## Model 5: Scrum Events

Read the section `Scrum Events` and answer the following questions.

Time: 30m

Scrum Events

1. What are the major Scrum events? 

sprint review, daily scrum, sprint retrospective, and scrum artifacts. sprint

2. What is the fundamental purpose of all Scrum events?

to keep things organized and mantianed daily

Sprints

3. How long is a typical Sprint?

2-4 weeks

4. True or false; the length of a Sprint is determined by the amount of work that needs to be done? **false**
5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint? **false**
6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent the Team from meeting the Spring Goal on time. Which of the following is allowed by Scrum?
   1. Renegotiate the end of the current Sprint?
   2. Renegotiate the quality of the work being done in the current Sprint?
   3. **Renegotiate the scope of the work of the current Sprint?**
7. When may a Sprint be canceled and by whom?

when a goal becomes obsalet the product owner

**Sprint Planning**

1. Purpose?

intitates the sprint by laying out the work

2. Length?

about 8 hours

3. Frequency?

at the begining of each sprint

4. When during the Sprint?

at the begining of each sprint

5. Who are involved?

the scrum team

6. What is the input?

the product backlog, latest product incrument, and past preformance of the development team

7. What is the output?

the sprint goal and sprint backlog

**Daily Scrum**

1. Purpose?

Inspect progress toward the Sprint Goal and adapt the Sprint Backlog.

2. Length?

Time-boxed to 15 minutes.

3. Frequency?

Daily, during the Sprint

4. When during the Sprint?

everyday

5. Who are involved?

the scrum team

6. What is the input?

The work completed since the last Daily Scrum, the work planned for the next 24 hours, and any impediments.

7. What is the output?

Updated Sprint Backlog.

**Sprint Review**

1. Purpose?

Inspect the Increment and adapt the Product Backlog.

2. Length?

Typically, 4 hours for a monthly Sprint.

3. Frequency?

At the end of each Sprint.

4. When during the Sprint?

At the end of each Sprint.

5. Who are involved?

Scrum Team and the stakeholders.

6. What is the input?

Increment, Product Backlog.

7. What is the output?

Revised Product Backlog.

Sprint Retrospective

1. Purpose?

Inspect how the last Sprint went with regards to people, relationships, process, and tools.

2. Length?

Typically, 3 hours for a monthly Sprint.

3. Frequency?
4. When during the Sprint?
5. Who are involved?
6. What is the input?
7. What is the output?

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 6: Scrum Artifacts

Time: 30m

Read the section `Scrum Artifacts` and its subsections and answer the following questions.

Product Backlog

1. What is it and what does it contain?
2. Who is responsible for sizing an item in the backlog?
3. Who is responsible for ordering the items in the backlog (highest priority first)?
4. What is the Product Goal and its purpose?

Sprint Backlog

1. What is in the Sprint Backlog?
2. What is the Sprint Goal and its purpose?

Increment
1. What is it?
2. How many may be produced as part of a sprint?
3. What is the Definition of Done?
4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
