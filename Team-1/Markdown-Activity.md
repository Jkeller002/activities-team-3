# Markdown Activity

Time: 15m

## Roles

- Manager: NATE L
- Recorder: ERIC P
- Spokesperson: ANTONIO L

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown?
2. What is CommonMark?
3. What is GitHub Flavored Markdown?
4. What kind of file do you use to create Markdown?
5. In Markdown How do you create
    1. A heading?
    2. A list?
    3. A sub-list?
    4. An ordered list?
    5. A pre-formatted block?
    6. Make something bold?
    7. Make something italic?
    8. Make something both bold and italic?
    9. Link to something?
    10. Add a horizontal rule (bar or separator)?
6. Besides VSCode, how can you view a rendered Markdown file?

(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.
