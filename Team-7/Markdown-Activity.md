# Markdown Activity

Time: 15m

## Roles

- Manager: Rob
- Recorder: Mason
- Spokesperson: Mason

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown?
A: An easy to read and easy to write language for formatting plain text. To format your writing on github for repositories, readmes and comments on pull requests and issues.
2. What is CommonMark?
A: A strongly specified implimentation of markdown
3. What is GitHub Flavored Markdown?
A: A rationalized version of Markdown with some different syntax but is mostly the same.
4. What kind of file do you use to create Markdown?
A: The file would have an extension of .md
5. In Markdown How do you create
    1. A heading?
    A: Add number signs in front of a word or phrase (#)
    2. A list?
    A: Start each line with - or *
    3. A sub-list?
    A: Indent each item with four spaces
    4. An ordered list?
    A: Add 1. to the start of the list items
    5. A pre-formatted block?
    A: Four spaces indent or backticks (```)
    6. Make something bold?
    A: Add double * to the front and back
    7. Make something italic?
    A: Add single * to the front and back
    8. Make something both bold and italic?
    A: Add triple *** to the front and back
    9. Link to something?
    A: Put the url inside square brackets []
    10. Add a horizontal rule (bar or separator)?
    A: Three dashes -
6. Besides VSCode, how can you view a rendered Markdown file?

(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.
