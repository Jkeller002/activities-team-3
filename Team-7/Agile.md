# Agile

## Roles

* Manager: MA
* Recorder: RR
* Spokesperson: RR
* Researcher: NR


## Model 1 - What is Agile?

12m Video: *What is Agile?* by Mark Shead. May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

Questions: 5m

1. Is Agile a process?
A. No
2. Is Agile a framework?
A. No
3. Is Agile a set of tools?
A. No
4. What is Agile?
A. Agile is a set of values and principles
5. What does Agile help companies and developers do?
A. Helps companies make decisions on how to develop software. 
Class discussion: 5m


## Model 2 - The Agile Manifesto

Time: 10m

At the core of The Agile Manifesto are four value statements.
They are structured as follows:

```
We value

    Individuals and Interactions  over  processes  and tools

    Working  software  over  comprehensive documentation

    Customer  collaboration  over  Contract negotiation

    Responding to change  over  following a plan

```
The missing phrases are mixed up below. Unscramble them to reconstruct the
value statements of the Agile Manifesto. Try to do it without looking up the Manifesto.

* `Individuals`
* `Working`
* `Responding`
* `Customer`
* `documentation`
* `and tools`
* `negotiation`
* `collaboration`
* `following`
* `and interactions`
* `software`
* `comprehensive`
* `proccess`
* `to change`
* `a plan`
* `contract`

Once you are satisfied, check your work against [The Agile Manifesto](https://www.agilealliance.org/agile101/the-agile-manifesto/) . How many statements did your team get right (ignoring the order of the statements)?

1. Does the Manifesto imply that projects should not use tools or follow a process?
A. No
2. Does the Manifesto imply that projects should not write any documentation?
A. No
3. Does the Manifesto imply that projects should not have contracts with clients?
A. No
4. Does the Manifesto imply that projects should not create or follow a plan?
A No
5. Based on their values, why would an Agile team reject the waterfall method to developing software?
A. ecause the waterfall method puts emphasis on all the things that an Agile team doesn't put much thought into

Class discussion: 10m


## Model 3

Time: 20m

Below are the 12 Agile principles that embody the 4 Agile value statements.
For each, identify which value statement the principle supports or is most related to,
and be prepared to explain how.

1. Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
A. Customer  collaboration  over  Contract negotiation, because it is describing the interaction between the customer to collaborate on the exact software that they want.

2. Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
A. Responding to change  over  following a plan, it literally is describing being adapatable to any challenge.

3. Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
A. Working  software  over  comprehensive documentation, as they want to able to provide a working software from any state of development.

4. Business people and developers must work together daily throughout the project.
A. Individuals and Interactions  over  processes  and tools, a team effort thats cohesive rather than splitting up due to a specific plan.

5. Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
A. Individuals and Interactions  over  processes  and tools, this team values the individuals in it rather than forcing them into an environment that is stilfling success.

6. The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
A.  Individuals and Interactions  over  processes  and tools, they value the conversations that lead to new development

7. Working software is the primary measure of progress.
A. Working  software  over  comprehensive documentation, as they want to provide a working software above all else.

8. Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
A. Individuals and Interactions  over  processes  and tools, because it talks about keeping everyone involved and in the loop at all times

9. Continuous attention to technical excellence and good design enhances agility.
A. Working  software  over  comprehensive documentation, because if you are keeping softwar working and good documentation it will lead to faster times.

10. Simplicity--the art of maximizing the amount of work not done--is essential.
A. Responding to change  over  following a plan, because it is basically saying that simplicity can take out the unessary parts of it and only what is needed. This is better than doing it a certain way even if it was the original plan.

11. The best architectures, requirements, and designs emerge from self-organizing teams.
A.  Individuals and Interactions  over  processes  and tools, these interactions lead to a self-organizing team that can adapt to anything.

12. At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.
A. Responding to change  over  following a plan, instead of sticking to a plan no matter what but instead look at what changed and how to tackle said changes.
Class discussion: 20m


## References

* 12m Video: *What is Agile?* by Mark Sheed May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

* The Agile Manifesto

  * Values

    * Original: <https://agilemanifesto.org/>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/the-agile-manifesto/>

  * Principles

    * Original: <https://agilemanifesto.org/principles.html>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/>


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
