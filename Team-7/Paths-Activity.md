# Paths - Activity

There are several tools that provide graphical interfaces for working
with Git. However, if you ever run into a problem and look for help online,
that help will likely give you instructions to run on the command-line.
That's because Git's primary interface is the command-line. For this reason,
you'll learn to use Git from the command-line. With this knowledge you'll
be able to easily learn any graphical interface for Git.

We begin with an introduction to the command-line, and we start with paths.
Paths allow us to identify files and directories in a filesystem from the
command-line.

## Roles

* Manager: RR
* Recorder: NR
* Spokesperson: MA
* Quality Control: MA


## Model 1 - Absolute Paths

Time: 15m

### File system

> :information_source: The following diagram must be rendered by PlantUML. GitLab and GitHub should render it. Your previewer may not.

```plantuml
@startmindmap
*:
  C:\ (Windows)
  /c/ (Windows Git-Bash)
  / (Linux or MacOS)
;
** System
** Users
*** Ant
**** Documents
**** Desktop
***** cs220
******_ README.md
****_ .gitconfig
*** Bat
**** Code
*** Cat
** Temp
@endmindmap
```

### Example absolute paths

| OS | Absolute path to the README.md file |
| -- | ----------------------------------- |
| Linux or MacOS | `/Users/Ant/Desktop/cs220/README.md` |
| Windows | `C:\Users\Ant\Desktop\cs220\README.md` |
| Windows Git-Bash | `/c/Users/Ant/Desktop/cs220/README.md` |

### Questions

In Linux or MacOS...

1. What character is used to separate components in a path?
A. / seperates the components in a path
2. What character represents the root of the filesystem?
A.  / represents the root of the filesystem.
3. An absolute path always begins with what character?
A. / ia the beginning character
4. Write an absolute path to the `.gitconfig` file.
A. /Users/Ant/Desktop/cs220/Team-7/.gitconfig
5. Write an absolute path to the `Cat` directory.
A./Users/Cat

6. Does `/Bat/Code` exist in the given filesystem?
A. yes /Bat/Code does exist
In Windows...

7. What character is used to separate components in a path?
\
8. What characters represents the root of the filesystem?
A.  'C:/' represents the root of the filesystem
9. An absolute path always begins with what character?
A. 'C' is the starting character 
10. Write an absolute path to the `.gitconfig` file.
A.  C:\Users\Ant\.gitconfig
11. Write an absolute path to the `Cat` directory.
A.  C:\Users\Cat

In Git-Bash...

12. What character is used to separate components in a path?
A. / is used to seperate components in a path
13. What characters represents the root of the filesystem?
A.  /c/ represents the root of the filesystem
14. An absolute path always begins with what character?
A. / is the starting character of an absolute path
15. Write an absolute path to the `.gitconfig` file.
A.  /c/Users/Ant/.gitconfig
16. Write an absolute path to the `Cat` directory.
A.  /c/Users/Cat

> **STOP** and review as a class

Discussion: 8m

## Model 2 - Relative Paths

Time: 15m

### File system

> :information_source: The following diagram must be rendered by PlantUML. GitLab and GitHub should render it. Your previewer may not.

```plantuml
@startmindmap
*:
  C:\ (Windows)
  /c/ (Windows Git-Bash)
  / (Linux or MacOS)
;
** System
** Users
*** Ant
**** Documents
**** Desktop
***** cs220
******_ README.md
****_ .gitconfig
*** Bat
**** Code
*** Cat
** Temp
@endmindmap
```

### Example relative paths

| Absolute path to current directory | Relative path to README.md |
| ----------------------------------- | -------------------------- |
| `/Users/Ant/Desktop/cs220` | `README.md` |
| `/Users/Ant/Desktop/cs220` | `./README.md` |
| `/Users/Ant` | `Desktop/cs220/README.md` |
| `/Users/Ant/Documents` | `./../Desktop/cs220/README.md` |
| `/Users/Ant/Documents` | `../Desktop/cs220/README.md` |

### Questions

In Linux or MacOS...

1. What character do relative paths never begin with?
A. never begins with the root 
2. Describe what a relative path may begin with.
A. file name or directory name of where you are
3. What do you think `.` represents?
A. the currrent directory you are in 
4. Is `.` required?
A. No "." is not always required
5. What do you think `..` represents?
A. parent directory of the current directory 
6. Suppose the current directory is `/Users/Ant/Desktop`, write a relative path to...
   1. `README.md`
   A.  ./CS220/README.md
   2. `.gitconfig`
   A. ./../.gitconfig
   3. `Cat`
   A. ./../../Cat
7. Repeat the last question for Git-Bash
   1. `README.md`
   A.  ./CS220/README.md
   2. `.gitconfig`
   A. ./../.gitconfig
   3. `Cat`
     A. ./../../Cat
8. Repeat the last question for Windows
   1. `README.md`
  A.  .\CS220\README.md
   2. `.gitconfig`
    A. .\..\.gitconfig
   3. `Cat`
       A. .\..\..\Cat

> **STOP** and review as a class

Time: 8m

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2021, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
