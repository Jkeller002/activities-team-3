# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter:RR
* Recorder & Pilot:NR
* Quality Control:RR
* Efficiency Expert:MA

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea?
A. No because they are not under the copyright law.

2. You have written a new program based on that idea, is that software copyright-able?
A. Yes because copyright is automatically given to the creator of the work.

3. You have written a new program, what do you have to do to copyright that program?
A.  You have to apply for a copyright license to have it enforced but it is automatic.

4. If you own the copyrights to something, what can you do that no one else can do? (see <http://www.bitlaw.com/copyright/scope.html> )
A. You have the right to reproduce the copyrighted work,  prepare derivative works based upon the work,distribute copies of the work to the public, perform the copyrighted work publicly, and to  display the copyrighted work publicly

5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?
A. It is automatically copyrighted so you can't legally do anything with it as you haven't been given permission.

6. What is the purpose of a license?
A.  It grants people permissions that wouldn't normally have them like to use, modify, and to share, or an obligation

7. When you license your program do you give up your copyright?
A. No becuase you are just giving other people rights to also use your work but not giving up the rights to it

8. As the copyright holder, once you license your program, could you later license it under a different license?
A. Yes you can change the license as the copyright holder

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license?
A. Only as long as it was made before the new license was issued

10. Can you sell open-source licensed software?
A. No because open-source is free, unless it is redhat, so you can buy services revolving around the open-source work.

11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?
A. The company owns the copyright, and they choose what the license is not you.

12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?
A. Both own the copyright as it is a joint work

13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?
A. You own the copyright but you can give your friend a license to work under.

14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core>
    2. <https://github.com/apache/fineract>
    3. Find VSCodium on GitHub.

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
