# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: MA
* Recorder: NR
* Spokesperson: NR
* Researcher: RR


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition, Theory, and Values

Time: 25m

Read the section `Scrum Definition` and answer the following questions.

1. True or false; you can apply Scrum as described in The Scrum Guide.
A. True
2. True or false; you should not deviate from Scrum as described in The Scrum Guide.
A. False
3. True or false; Scrum is a completely defined process for software development.
A.  False
Read the section `Scrum Theory` and answer the following questions.

1. What are the three pillars of Scrum?
A. Transparency , Inspection, Adaption, 
2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?
A. it would remove inspection because otherwise it would b emisleading and wasteful
3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?
A. Inspection enables adaption 
4. What is necessary for "adaptation" to be possible?
A. it requires an empowered , or self-managing people to be adaptable, inspection, and transparency
Read the section `Scrum Values` and answer the following questions.

1. What values does Scrum add to the Agile values?
A. respect , courage , openness and commitment
2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 4: Scrum Team

Time: 20m

Read the section `Scrum Team` and answer the following questions.


1. What roles are there in the Scrum Team?
A. Scrum master , product owner, developers
2. How big should a Scrum Team be?
A. small team of 10 people or less
3. Which of these teams is cross-functional?
   1. A database team that is able to design and implement the database.
   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert.
   A. Team 2 would the cross-functional
4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog.
   A. developers
   2. Clearly communicates items in the Product Backlog.
   A. Product Owner
   3. Ensures Scrum events take place and are positive, productive, and time-boxed.
   A. Scrum Master
   4. Orders/prioritizes the items in the Product Backlog.
   A. Product Owner
   5. Removes barriers between stakeholders and the Scrum Team.
   A. Scrum Master
   6. Holds the team accountable as professionals.
   A.  Developers
   7. Adhering to the Definition of Done.
   A. Developer
   8. Adapting the plan each day toward the Sprint Goal.
   A. Developers
   9. Clearly communicates the Product Goal.
   A. Product Owner

---

> ***STOP*** Review as a class before continuing.

Time: 10m

---

## Model 5: Scrum Events

Read the section `Scrum Events` and answer the following questions.

Time: 30m

Scrum Events

1. What are the major Scrum events?
A.  The Sprint, Sprint Planning, Daily Scrum, Sprint review, Sprint Retrospective, Scrum Aritifact 
2. What is the fundamental purpose of all Scrum events?
A. The purpose is to creat regularity and minimize need for meetings not defined in Scrum

Sprints

3. How long is a typical Sprint?
A. One month or less 
4. True or false; the length of a Sprint is determined by the amount of work that needs to be done?
A. False
5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint?
A. False
6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent 
the Team from meeting the Spring Goal on time. Which of the following is allowed by Scrum?
   1. Renegotiate the end of the current Sprint?
   2. Renegotiate the quality of the work being done in the current Sprint?
   3. Renegotiate the scope of the work of the current Sprint?
   A. 2 and 3 are allowed by Scrum
7. When may a Sprint be canceled and by whom?
A. It can only be cancelled if the sprint goal is obscelete and only by the product Owner
Sprint Planning

1. Purpose?
A.  initiates the Sprint by planning 
2. Length?
A. 8 Hours Maximum
3. Frequency?
A. Once a Month
4. When during the Sprint?
A. Before and after
5. Who are involved?
A. Product Owners, Developers, and whoever else needed to be included
6. What is the input?
A. Product Backlog
7. What is the output?
A. Product backlog made into smaller increments

Daily Scrum

1. Purpose?
A. Inspect progress of the sprint goal and adapt to the product backlog
2. Length?
A. 15 Minutess
3. Frequency?
A. Everyday
4. When during the Sprint?
A. Full Duration of SPrint
5. Who are involved?
A. Developers
6. What is the input?
A. what happened yesterday 
7. What is the output?
A. A plan for the day to meet the goal

Sprint Review

1. Purpose?
A. inspect the outcome of the sprint and determine future adaptations
2. Length?
A. 4 Hours 
3. Frequency?
A. Once 
4. When during the Sprint?
A. Second to last
5. Who are involved?
A. Scrum Team
6. What is the input?
A. the Sprint that is about to finish
7. What is the output?
A. a plan to make it better

Sprint Retrospective

1. Purpose?
A. plan ways to increase effecienty and quality
2. Length?
A. 3 Hours max
3. Frequency?
A. Once
4. When during the Sprint?
A. Final thing in the sprint
5. Who are involved?
A. Scrum Team
6. What is the input?
A. what went wrong and what went great during the sprint
7. What is the output?
A. the most helpful changes to improve effectivness

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 6: Scrum Artifacts

Time: 30m

Read the section `Scrum Artifacts` and its subsections and answer the following questions.

Product Backlog

1. What is it and what does it contain?
2. Who is responsible for sizing an item in the backlog?
3. Who is responsible for ordering the items in the backlog (highest priority first)?
4. What is the Product Goal and its purpose?

Sprint Backlog

1. What is in the Sprint Backlog?
2. What is the Sprint Goal and its purpose?

Increment
1. What is it?
2. How many may be produced as part of a sprint?
3. What is the Definition of Done?
4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
