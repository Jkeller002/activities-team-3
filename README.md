# Activities

## Overview

Activities are organized by the exam. They are the source material for
much of this course. They serve as communal notes for your team and the
course. When it comes time to study for the exam, you should study the
completed activities of your and/or other teams.

## Setup

1. Give your GitLab username to your instructor.
2. Your instructor will give you "maintainer" privileges over a group
    on GitLab containing your team's fork (and possibly other teams' forks).

> **Why Maintainer?**
>
> This will allow you to synchronize your team's fork's main with upstream's.
> It will also allow you to create new projects and subgroups in that group.
> However, you won't be able to delete projects and subgroups. If you need
> something deleted, contact your instructor.
>
> Here is more information on [Permissions and roles in GitLab](https://docs.gitlab.com/ee/user/permissions.html)

## Process

To work on an activity, have your recorder do the following:

1. Open your team's fork of this project in GitPod.
2. Synchronize main in your team's fork with upstream's.
3. Create a feature branch for today's activity.
4. Copy today's activity to your team's folder.
5. Edit your team's copy in your team's folder.

When you are done working

1. Stage and commit your changes.
2. Publish your team's feature branch to your team's fork.
3. Create a merge-request back to upstream.

Your instructor will merge your MR before the next class session unless
your team modified the original file, or there is a conflict (i.e., you
probably edited another team's activity). If there is a conflict,
you're team will need to resolve it, or continue from another team's work.
