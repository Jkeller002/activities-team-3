# Markdown Activity

Time: 15m

## Roles

- Manager:Meraj Husen
- Recorder:Jason Desantis
- Spokesperson:Bidhyaram Gupta

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown?
Markdown is a simple markup language used to format plain text for easy conversion into HTML and other formats.

2. What is CommonMark?
CommonMark is a standardized version of Markdown aimed at creating consistent rules for its syntax across different platforms and tools. It ensures that Markdown documents are parsed and rendered consistently, improving interoperability and portability.

3. What is GitHub Flavored Markdown?

GitHub Flavored Markdown (GFM) is a modified version of Markdown tailored for GitHub, adding features like task lists, user and issue mentions, automatic linking, syntax highlighting for code blocks, and tables.

4. What kind of file do you use to create Markdown?
Markdown files typically use the file extension ".md" or ".markdown". For example, a Markdown file might be named "example.md" or "README.md". These file extensions help identify the content as being written in Markdown format.

5. In Markdown How do you create
    1. A heading?
    To create a heading in Markdown, use hash symbols (#) followed by a space and the text of the heading. The number of hash symbols indicates the heading level. For example:
    ### head
    2. A list?
    
To create a list in Markdown:

For an unordered list, use -, *, or + followed by a space and the item content.
For an ordered list, use numbers followed by a period (1., 2., etc.) and a space.
    *
    *
    *
    3. A sub-list?
    To create a sublist in Markdown, indent the list items beneath the parent item using spaces or tabs. Here's an example:
    - Main item
       - Subitem 1
       - Subitem 2

    4. An ordered list?
    To create an ordered list in Markdown, you use numbers followed by a period (1., 2., etc.) and a space before each list item. Here's an example:
    1. Item 1
    2. Item 2
    3. Item 3

    
    5. A pre-formatted block?

    To create a pre-formatted block in Markdown, use triple backticks ``` or indent each line with four spaces.

    6. Make something bold?
    To make text bold in Markdown, you can use double asterisks ** or double underscores __ around the text you want to emphasize. Here's an example:
    **Bold Text**
    __Bold Text__


    7. Make something italic?

    To make text italic in Markdown, you can use either single asterisks * or single underscores _ around the text you want to emphasize. Here's an example:
    *Italic Text*
    _Italic Text_

    8. Make something both bold and italic?
    To make text both bold and italic in Markdown, you can combine the syntax for bold and italic by enclosing the text with three asterisks *** or three underscores ___. Here's an example:
    ***Bold and Italic Text***
   ___Bold and Italic Text___


    9. Link to something?
    To create a link in Markdown, you enclose the text you want to display for the link in square brackets [], followed by the URL enclosed in parentheses (). Here's an example:
    [GitP0D website](https://wnecsitjack-team5activi-3nc6s4c9bhf.ws-us108.gitpod.io/)

    10. Add a horizontal rule (bar or separator)?
    To add a horizontal rule (also known as a horizontal line, bar, or separator) in Markdown, you can use three or more hyphens (---), asterisks (***), or underscores (___) on a line by themselves. Here's an example using hyphens:
    ---

6. Besides VSCode, how can you view a rendered Markdown file?
You can view a rendered Markdown file using online Markdown editors, Markdown preview plugins/extensions in text editors, standalone Markdown preview tools, or by opening the file directly in a web browser.

(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.
