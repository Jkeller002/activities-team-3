# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter:Jason Desamtis
* Recorder & Pilot: Meraj Husen
* Quality Control: Bidhyaram Gupta
* Efficiency Expert:

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea?

No. We can not.

2. You have written a new program based on that idea, is that software copyright-able?
Yes.


3. You have written a new program, what do you have to do to copyright that program?
- it will automatically copyright that program  for us.

4. If you own the copyrights to something, what can you do that no one else can do? (see <http://www.bitlaw.com/copyright/scope.html> )
- the right to reproduce the copyrighted work
the right to prepare derivative works based upon the work
the right to distribute copies of the work to the public
the right to perform the copyrighted work publicly
the right to display the copyrighted work publicly


5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?
-it depents on the country law. In united states you can us as public domain.
- we can just open it; see it; but can't downloard; can't compile

6. What is the purpose of a license?
Grant right people would not usually have:
to use
to modify 
to share
create obligations:
attribution
share-alike
whatever shows up in a proprietary license



7. When you license your program do you give up your copyright?
no. You retain your copyright.
8. As the copyright holder, once you license your program, could you later license it under a different license?
yes

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license?
Yes

10. Can you sell open-source licensed software?
yes

11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?
- company owns the copyright. whatever the company decide would be the license for that copyright.

12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?

Joint Work: If contributions can't be separated and all intended a single final product, it's a "joint work" with joint ownership (equal rights unless agreed otherwise).
Independent Works: If contributions are separable, each contributor owns their part.

MIT license requires the copyright holder's permission.
For "joint work," all joint owners must agree.
Get written agreements to ensure clarity and avoid future disputes. 

13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?
Add full license text & copyright/license notices in each file.
Make source code readily available (e.g., on GitHub).
They own their code, but license it under GPLv3.0 with your project.
You can use freely, but respect their copyright (no claiming ownership).


14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core>OpenMRS: MPL 2.0 with HD © OpenMRS Inc.
    2. <https://github.com/apache/fineract>Apache-2.0 license.
    3. Find VSCodium on GitHub.MIT License

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.
-

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?
Permissive:

Freedom to use, modify, and distribute (even commercially) with few restrictions.
Code changes can stay proprietary (closed-source).
Example: MIT License.
Non-permissive/CopyLeft:

Same freedoms as permissive, but...
Modified code must also be open-source and shared under the same license.
Promotes open-source development but may limit commercial use.
Example: GPL.

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
