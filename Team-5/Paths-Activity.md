# Paths - Activity

There are several tools that provide graphical interfaces for working
with Git. However, if you ever run into a problem and look for help online,
that help will likely give you instructions to run on the command-line.
That's because Git's primary interface is the command-line. For this reason,
you'll learn to use Git from the command-line. With this knowledge you'll
be able to easily learn any graphical interface for Git.

We begin with an introduction to the command-line, and we start with paths.
Paths allow us to identify files and directories in a filesystem from the
command-line.

## Roles

* Manager: Bidhyaram Gupta
* Recorder: Jason DeSantis
* Spokesperson: Meraj Husen
* Quality Control: Bidhyaram Gupta


## Model 1 - Absolute Paths

Time: 15m

### File system

> :information_source: The following diagram must be rendered by PlantUML. GitLab and GitHub should render it. Your previewer may not.

```plantuml
@startmindmap
*:
  C:\ (Windows)
  /c/ (Windows Git-Bash)
  / (Linux or MacOS)
;
** System
** Users
*** Ant
**** Documents
**** Desktop
***** cs220
******_ README.md
****_ .gitconfig
*** Bat
**** Code
*** Cat
** Temp
@endmindmap
```

### Example absolute paths

| OS | Absolute path to the README.md file |
| -- | ----------------------------------- |
| Linux or MacOS | `/Users/Ant/Desktop/cs220/README.md` |
| Windows | `C:\Users\Ant\Desktop\cs220\README.md` |
| Windows Git-Bash | `/c/Users/Ant/Desktop/cs220/README.md` |

### Questions

In Linux or MacOS...

1. What character is used to separate components in a path?
    The character is a /
2. What character represents the root of the filesystem?
   The root in Linux and MacOS is /
3. An absolute path always begins with what character?
   The absolute path begins with /
4. Write an absolute path to the `.gitconfig` file.
   /Users/Ant/.gitconfig
5. Write an absolute path to the `Cat` directory.
   /Users/Cat
6. Does `/Bat/Code` exist in the given filesystem?
    Yes

In Windows...

7. What character is used to separate components in a path?
    The character is a \
8. What characters represents the root of the filesystem?
    The root of the filesystem is represented by the drive letter, followed by a colon. e.g C:
9. An absolute path always begins with what character?
    The absolute path begins with :
10. Write an absolute path to the `.gitconfig` file.
    C:\Users\Ant\.gitconfig
11. Write an absolute path to the `Cat` directory.
    C:\Users\Cat

In Git-Bash...

12. What character is used to separate components in a path?
    The character is /
13. What characters represents the root of the filesystem?
    The root is /c/
14. An absolute path always begins with what character?
    The absolute path always begins with /
15. Write an absolute path to the `.gitconfig` file.
    /c/Users/Ant/.gitconfig
16. Write an absolute path to the `Cat` directory.
    /c/Users/Cat

> **STOP** and review as a class

Discussion: 8m

## Model 2 - Relative Paths

Time: 15m

### File system

> :information_source: The following diagram must be rendered by PlantUML. GitLab and GitHub should render it. Your previewer may not.

```plantuml
@startmindmap
*:
  C:\ (Windows)
  /c/ (Windows Git-Bash)
  / (Linux or MacOS)
;
** System
** Users
*** Ant
**** Documents
**** Desktop
***** cs220
******_ README.md
****_ .gitconfig
*** Bat
**** Code
*** Cat
** Temp
@endmindmap
```

### Example relative paths

| Absolute path to current directory | Relative path to README.md |
| ----------------------------------- | -------------------------- |
| `/Users/Ant/Desktop/cs220` | `README.md` |
| `/Users/Ant/Desktop/cs220` | `./README.md` |
| `/Users/Ant` | `Desktop/cs220/README.md` |
| `/Users/Ant/Documents` | `./../Desktop/cs220/README.md` |
| `/Users/Ant/Documents` | `../Desktop/cs220/README.md` |

### Questions

In Linux or MacOS...

1. What character do relative paths never begin with?
    The relative path never begin wth \
2. Describe what a relative path may begin with.
    The relative path may begin with / or ./ or the directory
3. What do you think `.` represents?
    . represents the current directory

4. Is `.` required?
    No, since some of the relative paths did not start with .
5. What do you think `..` represents?
    .. represents the parent directory
6. Suppose the current directory is `/Users/Ant/Desktop`, write a relative path to...
   1. `README.md`
       ./cs220/README.md
   2. `.gitconfig`
       ../.gitconfig
   3. `Cat`
      ../../Cat
7. Repeat the last question for Git-Bash
   1. `README.md`
       ./cs220/README.md
   2. `.gitconfig`
       ../.gitconfig
   3. `Cat`
       ../../Cat
8. Repeat the last question for Windows
   1. `README.md`
       .\cs220\README.md
   2. `.gitconfig`
       ..\.gitconfig
   3. `Cat`
       ..\..\Cat

> **STOP** and review as a class

Time: 8m

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2021, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
