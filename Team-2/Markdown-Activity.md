# Markdown Activity

Time: 15m

## Roles

- Manager: Tom
- Recorder: Argel
- Spokesperson: Nick

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown? Lightweigh mark up language that you can use for formatting elements to plaintext text documents
2. What is CommonMark? More specific verison of Markdown by specifying syntax
3. What is GitHub Flavored Markdown? Markdown that specifys to github
4. What kind of file do you use to create Markdown? a .md file
5. In Markdown How do you create
    1. A heading? add # infront of a word/phrase, the number of #s determines the level of heading
    2. A list? To create an ordered list, add line items with numbers followed by periods, and it needs to start with 1, but do not have to be in order
    3. A sub-list? when making a list to make an item indent, with a tab, the desired sub item, and use either * or -
    4. An ordered list? To create an unordered list, add dashes (-), asterisks (*), or plus signs (+) in front of line items. Indent one or more items to create a nested list.
    5. A pre-formatted block? a block can be made with either 4 spaces or a tab
    6. Make something bold? add two asterisks or underscores before and after a word/phrase
    7. Make something italic? add one asterisk or underscore before and after a word/phrase
    8. Make something both bold and italic? add asterisks or underscores before and after a word/phrase
    9. Link to something? enclose the text with brackes [] then follow with the link in paranthesis()
    10. Add a horizontal rule (bar or separator)? Three or underscores in a line by itself
6. Besides VSCode, how can you view a rendered Markdown file? If you're on MacOS use Mou

(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.
