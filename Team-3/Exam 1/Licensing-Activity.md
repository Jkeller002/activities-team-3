# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter: JG
* Recorder & Pilot: CR/JK
* Quality Control: JK/JG
* Efficiency Expert: CR

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea?
     The overall idea of the program itself can not be copyrighted but the material within it can as long as it qualifies.

2. You have written a new program based on that idea, is that software copyright-able?
    As long as my program has original code, artwork, or other creative elements, it can be eligble for copyright.

3. You have written a new program, what do you have to do to copyright that program?
   The process is generally automatic but you can,
    a. Include a copyright notice
    b. Keep good records
    c. Consider registration
    d. Use licenses
    e. Monitor infringement

4. If you own the copyrights to something, what can you do that no one else can do? (see <http://www.bitlaw.com/copyright/scope.html> )
    You have the right to reproduce the work, prepare derivative works, distribute copies, perform the work publicly, and display the work publicly.

5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?
    It's safe to assume that there a license attached to it and respect the integrety of the creator as well as their project.

6. What is the purpose of a license?
    It specifies the terms and conditions under which someone else can use, modify, distribute, or otherwise interact with a copyrighted work. It clarifies the rights and permissions granted by the copyright holder to others.

7. When you license your program do you give up your copyright?
    You do not give up your copyright. It means granting certain permissions to others while retaining ownership of the copyright.

8. As the copyright holder, once you license your program, could you later license it under a different license?
    You do have the authority to change the licensing terms for your program at any time but you can not revoke licenses that you have already granted.

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license?
    Someone can ideed use my program under the original license, unless I released a newer version of the program under a different license. The person would then have to adhere to this newly established license.

10. Can you sell open-source licensed software?
    You can sell open-source software. "Open-source" refers to the accessibilty of the code rather than the ownership of it. It also may depend on wht type of license being used for said software and what the contents of said license say.

11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?
    In most cases, the company is the one who owns the copyright for the software. The license depends on what the company agrees upon.

12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?
    The copyright is typically jointly owned but the collaborators involved in the project. In order to license the software under the MIT license, all contributors must agree to apply the license to the project, and make sure the text is included in the project repository.

13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?
    You must include a copy of the GPL-v3.0 licesne text in your project repository and make sure it is applied to all relevant files. The friend owns the copyright of whatever they write unless a different agreement is set in place.

14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core>
        Mozilla Public License 2.0
    2. <https://github.com/apache/fineract>
        Apache License 2.0
    3. Find VSCodium on GitHub.
        MIT License

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.

    Mozilla Public License 2.0: 
      Cans: Commercial Use, Modify, Distribute, Sublicense, Place Warranty, Use Patent Claims
      Cannots: Use Trademark, Hold Liable
      Musts: Include Copyright, Include License, Disclose Source, Include Original

    Apache License 2.0:
      Cans: Commercial Use, Modify, Distribute, Sublicense, Place Warranty, Private Use, Use Patent Claims
      Cannots: Use Trademark, Hold Liable
      Musts: Include Copyright, Include License, State Changes, Include Notice

    MIT License:
      Cans: Commercial Use, Modify, Distribute, Sublicense, Private Use
      Cannots: Hold Liable
      Musts: Include Copyright, Include License

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?
    Permissive licenses allow for greater freedom in using and incorporating the licensed code without imposing restrictions on the software. 
    Non-permissive licenses require the derivative works to be licensed under the same terms. This ensures that the freedoms granted by the original open-source license are preserved in distributions.

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
