# Markdown Activity

Time: 15m

## Roles

- Manager:
- Recorder: Jacob Keller
- Spokesperson:

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown?
    A lightweight markup language for creating formatted text using a plain-text editor. It is an unstandardized format.
2. What is CommonMark?
    A rationalized version of Markdown syntax with a spec whose goal is to remove the ambiguities and incosistency surronding the original Markdown specification. It is a standardized format.
3. What is GitHub Flavored Markdown?
    A variant of Markdown that extends the original Markdown syntax with additional features and conventions specific to GitHub's platform. These provide enhanced functionality for writing and formatting text on GitHub, particularly within issues, pull requests, and README files.
4. What kind of file do you use to create Markdown?
    .md files
5. In Markdown How do you create
    1. A heading? 
        Hash symbols (#). The amount determines the level of the heading
    2. A list?
        Astericks (*), plus signs (+), or hyphens (-)
    3. A sub-list?
        Similar to lists but just indented
    4. An ordered list? 
        Numbers followed by periods
    5. A pre-formatted block?
        Either triple backticks (```) or indent each line by four spaces
    6. Make something bold?
        Single *
    7. Make something italic?
        Double *
    8. Make something both bold and italic? 
        Triple *
    9. Link to something?
        [Link Name](URL)
    10. Add a horizontal rule (bar or separator)?
        Three or more hyphens (-), asterisks (*), or underscores (_). 
6. Besides VSCode, how can you view a rendered Markdown file?
    Online Markdown Editor, Markdown Preview Extensions, Markdown Viewer Web Browser, Command Line, Markdown Viewer Software

(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.
