# Agile

## Roles

* Manager: JG
* Recorder: JK/CR
* Spokesperson: JG
* Researcher: JK/CR


## Model 1 - What is Agile?

12m Video: *What is Agile?* by Mark Shead. May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

Questions: 5m

1. Is Agile a process?
    No

2. Is Agile a framework?
    No

3. Is Agile a set of tools?
    No

4. What is Agile?
    A set of values and principals

5. What does Agile help companies and developers do?
    It helps give a foundation in decision making to aid in software development.

Class discussion: 5m


## Model 2 - The Agile Manifesto

Time: 10m

At the core of The Agile Manifesto are four value statements.
They are structured as follows:

```
We value

    Individuals and Interactions  over  processes and tools

    Working software  over  comprehensive documentation

    Customer collaboration  over  contract negotiation

    Respoding to change  over following a plan

```
The missing phrases are mixed up below. Unscramble them to reconstruct the
value statements of the Agile Manifesto. Try to do it without looking up the Manifesto.

* `Individuals`
* `Working`
* `Responding`
* `Customer`
* `documentation`
* `and tools`
* `negotiation`
* `collaboration`
* `following`
* `and interactions`
* `software`
* `comprehensive`
* `proccess`
* `to change`
* `a plan`
* `contract`

Once you are satisfied, check your work against [The Agile Manifesto](https://www.agilealliance.org/agile101/the-agile-manifesto/) . How many statements did your team get right (ignoring the order of the statements)? All correct


1. Does the Manifesto imply that projects should not use tools or follow a process?
    No but individual interactions should be prioritzed before following a process.
2. Does the Manifesto imply that projects should not write any documentation?
    Agile encourages concise documentation but says to focus on the development of working software.
3. Does the Manifesto imply that projects should not have contracts with clients?
    The Manifesto implies collabrative interactions with customers over rigid contracts but they are both still necessary.
4. Does the Manifesto imply that projects should not create or follow a plan?
    It implies that plans should be adaptable to accommmodate changes to said plan.
5. Based on their values, why would an Agile team reject the waterfall method to developing software?
    An Agile team would reject the waterfall method because it is less collaborative and flexible for them and their customers.

Class discussion: 10m


## Model 3

Time: 20m

Below are the 12 Agile principles that embody the 4 Agile value statements.
For each, identify which value statement the principle supports or is most related to,
and be prepared to explain how.

1. Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
    This supports the "Working software over comprehensive documentation" statement. It supports the idea that delivering a usuable, quality software in order to get proper feedback in a timely manner. 

2. Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
    This supports the "Respoding to change over following a plan" statement. It agrees with the idea that plans should always be flexible and open to constructive criticism. 

3. Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
    This supports the "Working software over comprehensive documentation" statement. Making sure the company you work for has a working version of your software is important so that they may request changes or approve the current flow of the project.

4. Business people and developers must work together daily throughout the project.
    This supports the "Individuals and Interactions over processes and tools" statement. Having some sort of contract is important to any project but having frequent communication between both parties is is crucial for the betterment of the project. It is necessary to make sure that both parties agree on what the project looks like and make changes if there is a conflict.

5. Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
    This supports the "Individuals and Interactions over processes and tools" statement. It is important to communicate with developers about what they need to get their work done in a timely manner without much stress or other conflicts. If they have the proper resources they need, they feel more motivated to get their work done more efficiently.

6. The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
    This supports the "Individuals and Interactions over processes and tools" statement. It is easier to have a face-to-face conversation with someone over a concern rather than through email since there is no waiting for a response or risk of the message not getting to the individual.

7. Working software is the primary measure of progress.
    This supports the "Working software over comprehensive documentation" statement. Having working software is importnt to have in order to constantly put time into research and development. If the foucs is to make the software perfect on the first itteration, the software will never get to a working stage and be stuck in development.

8. Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
    This supports the "Customer collaboration over contract negotiation" or "Individuals and Interactions over processes and tools" statements. It is always important to make sure everyone involved in the project is on the same page for how things should go because it makes overall workflow easier.

9. Continuous attention to technical excellence and good design enhances agility.
    This could support the "Working software over comprehensive documentation" or "Respoding to change over following a plan" statements. Making sure the work done is precise and thorough is important for getting the work done on time as well as being easy enough for collaborators to understand.

10. Simplicity--the art of maximizing the amount of work not done--is essential.
    This could support the "Working software over comprehensive documentation" statement. Making sure that you look back at a problem and see what the simpliest way is to get it done is important. Keep the code as small and easy to understand as possible.

11. The best architectures, requirements, and designs emerge from self-organizing teams.
    This supports the "Individuals and Interactions over processes and tools" statement. A good foundation starts with good collaboration and a well-made design.

12. At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.
    This supports the "Individuals and Interactions over processes and tools" statement. Being able to reflect on previous work is important for a project so that the team can see what can be improved upon in the future.

Class discussion: 20m


## References

* 12m Video: *What is Agile?* by Mark Sheed May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

* The Agile Manifesto

  * Values

    * Original: <https://agilemanifesto.org/>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/the-agile-manifesto/>

  * Principles

    * Original: <https://agilemanifesto.org/principles.html>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/>


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
