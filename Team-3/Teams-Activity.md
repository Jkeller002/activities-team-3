# Teams Activity

Time: 15m

## Teams

Learning to work effectively in teams is one of the objectives of this course,
because teamwork is an essential skill in the modern software development workplace.
Also, learning in teams has been shown to be an extremely effective way to learn.
So in this course, you'll be working in small teams a lot.

## Roles and Responsibilities

When working in a team, it's good to have roles and responsibilities as it helps
the team work more effectively together.

In this class there are several roles that we'll use, some more commonly than others.
Below are some of the roles you'll encounter and their responsibilities.

- Manager - Manages time. Ensures everyone has a voice and is able to participate.
  Encourages others by periodically recognizing their achievements verbally.
- Recorder - Records the teams answers and notes from discussions and mini lectures.
- Spokesperson - Speaks for the team during full class discussions.
- Quality control - Ensures that the team's answers are good by questioning reasoning,
  rationale, and facts. Also known as the Skeptic.
- Efficiency expert - Ensures that the team is functioning well together,
  and makes notes about how the team might work more effectively in the future.
- Driver/Pilot - Manipulates code and tools that are being studied on behalf of the team.
- Navigator/Co-pilot - Reads the questions, stays aware of the big picture and what's next.
- Researcher - Responsible for finding relevant documentation online relevant to the task at hand.

Note that everyone is responsible for helping to work through activities and solve problems.

Each session/activity, your team will be asked to assign a set of roles to members.
Everyone will have at least one role, sometimes you may have more than one role.

Be sure to rotate roles each sessions so that everyone serves in different roles over time.

## Working as a team (in person)

- Place your Pilot in the middle (why?)
- Ensure your Recorder is the only one modifying your document (why?)
- Consider using Discord even when in person (why?)

## Teams on Discord

In the event that one or more of your team members must isolate/quarantine
or if you must work remotely as a team for some other reason, here is a
reasonable way to arrange your team on Discord.

- Recorder - The activity opened in an editable format.
- Pilot - The development environment.
- Other team members - Documentation and search results to help answer questions.

**All members share their screen.**

## Working on an Activity

All team members should work through each question together. Do not "divide and conquer"
the questions unless you have permissions to do so. The purpose is to use the team to
develop a shared understanding of the material under study. The process of doing this
will help all members develop a deeper understanding of the material while at the same
time improving everyone's interpersonal, communication, and teamwork skills.

---

Stop here and review with instructor.

---

## Activity

1. Form a triple or a pair if it's not possible to form a triple.
2. Sit next to each other so you can easily communicate.
3. Pick roles (you'll rotate them daily)
  * Three-person team:
    1. Person 1: (NAME)
      - Manager
      - Spokesperson
    2. Person 2: (NAME)
      - Pilot
      - Recorder
    3. Person 3: (NAME)
      - Researcher
      - Co-Pilot
  * Two-person team:
    1. Person 1: (NAME)
      - Manager
      - Spokesperson
      - Co-pilot
      - Researcher
    2. Person 2: (NAME)
      - Pilot
      - Recorder
4. Connect to Discord, Use Discord to share screens and pass notes to each other.
