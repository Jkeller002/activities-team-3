# Saving Changes

We will use git to keep track of changes made to the files in your
project.

Content Learning Objectives
---------------------------

*After completing this activity, students should be able to:*

-   Determine the state of files from Git's perspective.

-   Stage and commit files.

-   View the log of commits.

## Team Roles

* Manager: INITIALS
* Recorder: INITIALS
* Quality Assurance: INITIALS


## Model 1 (5 min)

In Git, a file can be in one of three states.

* **Modified:** The file has changes that *will not* be committed in the
    next **`git commit`** command.
* **Staged:** The file has changes that *will* be committed in the
    next **`git commit`** command.
* **Clean:** The file has no uncommitted changes.

```plantuml
@startuml

[*] -> Modified: create
Modified -> Staged: git stage
Staged -> Clean: git commit
Clean -> Modified: edit, rename, or delete
@enduml
```

### Questions

1. What operation will move a file from the Modified area to the Staged area?

2. What operation will move a file from the Staged to Clean?

3. Which operations are not Git commands?

4. For each non-Git operation, give a command line command to perform the
    operation or briefly explain how you would accomplish it.

> **Information:**
>
> `git stage` and `git add` are equivalent. Whenever possible we will
> use `git stage` as it better names what the command does. However,
> you will find a lot of documentation, including Git's, will often
> refer to `git add`. So it's important that you know the the two
> commands are the same.

## Model 2: Before adding Feature Y (10 min)

You are about to start making changes to add Feature Y to the program.
The current directory is under Git version control. You inspect its
state using `ls`, `git log`, and `git status` as follows.

```bash
$ ls
A.java  B.java  C.java

$ git log
commit 5f770c78b781bad12b43e2dc45a739290ba0c670 (HEAD -> main)
Author: Stoney Jackson <dr.stoney@gmail.com>
Date:   Thu Feb 22 16:57:26 2024 +0000

    Add Feature X

$ git status
On branch main
nothing to commit, working tree clean

$
```

### Questions

1.  Name the files in the current directory.

2.  What was the purpose of the last committed change to the project?

3.  Who made the last set of changes to the program?

4.  When did they finish making those changes?

5.  What does **`git log`** tell you about your program?

6.  What does **`git status`** tell you about your program?

7.  For each file in the program, place it in one of the three states below.

    * Modified:
    * Staged:
    * Clean:


## Model 3: Start work on Feature Y (5 min)

To add Feature Y, you make changes to **B.java**.

```bash
$ edit B.java

$ git status
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   B.java

no changes added to commit (use "git add" and/or "git commit -a")

$
```

> ℹ️ **Information**
>
> `edit` is not a real Linux command.
> When you see it in a trace, it means that the developer made
> changes to that file using their favorite editor.


### Questions

1.  For each file in the program, place it in one of the three states below.

    * Modified:
    * Staged:
    * Clean:


2.  Assume you are done with making changes to B.java. Looking only at the
    output in Model 3, what command can you run to add the file to the
    staging area?


3.  What is an equivalent command to your answer to the previous question?


## Model 4: Stage B.java (2 min)

You are happy with the changes you made **B.java**. There are other files
you need to change to implement Feature Y; so, you are not ready to commit
your changes. However, you want to make sure you don't forget to include the
changes you made to B.java in the next commit. So you stage B.java.

```bash
$ git stage B.java

$ git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   B.java

$
```

### Questions

1.  For each file in the program, place it in one of the three states below.

    * Modified:
    * Staged:
    * Clean:


## Model 5: Continue work on Feature Y (5 min)

To finish Feature Y, you change file A.java, and create a new file D.java.

```bash
$ edit A.java

$ edit D.java

$ git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   B.java

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   A.java

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        D.java

$
```

### Questions

1.  For each file in the program, place it in one of the three states below.

    * Modified:
    * Staged:
    * Clean:

2.  Suppose you want the next commit to include all of the changes to
    **A.java, B.java,** and **D.java**. What command(s) would you run to do this?


## Model 6: Done with Feature Y (10 min)

You have finished making all your changes to implement Feature Y,
and have Staged all the new and changed files. Let's commit them.

```bash
$ git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   A.java
        modified:   B.java
        new file:   D.java

$ git commit -m "Add Feature Y"
[main f419aed] Add Feature Y
 3 files changed, 2 insertions(+)
 create mode 100644 D.java

$ git status
On branch main
nothing to commit, working tree clean

$ git log
commit f419aedc8f1357aa36ebeb3858560035c5214aec (HEAD -> main)
Author: Stoney Jackson <dr.stoney@gmail.com>
Date:   Thu Feb 22 17:53:10 2024 +0000

    Add Feature Y

commit 5f770c78b781bad12b43e2dc45a739290ba0c670
Author: Stoney Jackson <dr.stoney@gmail.com>
Date:   Thu Feb 22 16:57:26 2024 +0000

    Add Feature X
```

### Questions

1.  What is the purpose of the first **`git status`** command?

2.  What does **`git commit`** do?

3.  What is the purpose of **`-m "Add Feature Y"`** in
    the **`git commit`** command?

4.  What is the purpose of the second **`git status`** command?

5.  What does the **`git log`** command demonstrate?

6.  In what order does **`git log`** shows commits?

7.  What is the purpose of the
    `commit f419aedc8f1357aa36ebeb3858560035c5214aec`
    part of the log for?

8.  Why do you think there is a stage? Why not go straight to commit?

## Model 7: Practice (40 min)

When working on a project, you will likely repeat the following over and over again.

```bash
edit ...                        # Make a change
git stage .                     # Stage the change
git commit -m "good message"    # Commit the change
```

Sometimes if you've made many changes and you want to be sure they all belong together before you commit

Manager, one at a time, have each of your team members work through the following exercise while the other team members assist. Be sure they share their screen. Have your recorder make notes of any insights your team discovers along the way.

This will take on average about 10 minutes per person. Probably longer for the first, and less for the last.

1. Create a new directory to hold a new project.
2. Initialize your new directory for use with git.

Use `git log` and `git status` before and after each of the following to confirm your understanding of what is going on.

3. Create a file, stage it, and commit it.
4. Create another file, stage it, and commit it.
5. Modify both files, stage ONE of them, and commit it.
6. Stage the other file and commit it.
7. Delete a file, stage the change, and commit it.
8. Rename a file, stage the change, and commit it.

---
Copyright © 2021 Karl R. Wurst and Stoney Jackson. This work
is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
