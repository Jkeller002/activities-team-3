# Git - Configuration

Git is very configurable. Git has some defaults, and tools like GitPod
may perform some additional configuration to make your life easier.
In this activity, you will explore some of the configuration options.


## Roles

* Recorder: INITIALS
* Driver: INITIALS
* Skeptic: INITIALS

## Model 1 - User Information

Time: 10m

Open a terminal running a linux-based shell with access to git (e.g., Git-Bash, Terminal.app, iTerm.app, or the terminal in VS Code) and follow along.

> :rotating_light: Privacy Warning! Read this section carefully so you can make an informed decision.

Probably the most important configuration is your identity.
Git stores your name and email in each commit you make.
This is how Git gives you credit for your contributions.
This information is easily accessible to anyone who has access to a
repository containing commits made by you.
It is common to push your commits to a public repository.
If you do, your name and email will be publicly available.
So choose a name and email that will publicly represent you.

1. Let's find out who git thinks you are. Run the following and record its output.

    ```bash
    git config --global user.name
    git config --global user.email
    ```

2. Change your name and email to something else.

    ```bash
    git config --global user.name "Your Name"
    git config --global user.email "your.email@someservice.com"
    ```

3. What commands would you run to confirm the change was made?
    Use them to confirm the change was made.


This information is NOT used to authenticate you to Git or any other service.
However, services like GitHub and GitLab will use this information
(specifically the email) to give you credit for your commit by looking for an
account that uses the same email and name. It's possible to add additional
emails to your GitHub or GitLab account. That way, you can use one email
for notifications, and a separate "throw away" email for commits.



> **Info: Working on a shared computer**
>
> If you are work on a shared computer, as in a public lab, you should
> clear these values before you walk away, by setting them to some arbitrary value.

> **GitPod**
>
> GitPod will set your name and email for you. It's possible to configure this,
> but it will take some research and work. Start with GitPod's documentation.

> **Info: `--global`**
>
> The `--global` sets this configuration for every repository
> **you** create on the current machine. Alternatively, if you replaced
> this with `--system` it would set this configuration for the entire system;
> and if you left it off, it sets it for the current repository
> (based on your current working directory).

4. Configure your `git` with your identity and verify that it has the right values now.

## Model 2 - Help

Demo: 5m

Git comes with built in documentation. To get detailed documentation on
a command, enter `git help COMMAND` where `COMMAND` is the command you
are interested in. To get more information about `config`, enter the following:

```bash
git help config
```

This documentation is so long that git is using a "pager" application
(usually `less`) to display the documentation a page at a time. Many
command-line applications use this pager, so it's worth learning how to
navigate it. To move a page forward, press the space-bar. To move a page
back, press `b`. To search for a word type `/someword` and then `n` to step
through each occurrence. To exit, type `q`.

In Linux, you can get help on most top-level commands like `less` and
`git` using the `man` command (short for "manual"). For more information
about `less`, try `man less`. Or for more information on `man` itself:
`man man`. Note that `man` also uses `less` to page its results. Git it
a try now.

## Model 3 - Default Branch

Demo: 5m

Branches are used to track a chain of commits. The default branch has
traditionally been named `master`, and much of the documentation on the
Web currently refers to this name. However, services like GitHub and GitLab
now support the more inclusive term `main`, and Git now makes it easy to
set the default branch name for any repositories we create.
Let's make this change now.

Confirm that your git installation is configure to use `main` as the default branch.

```bash
git config --global init.defaultbranch
```

If the result did not say `main`, set it to main as follows.

```bash
git config --global init.defaultbranch main
```

## Model 4 - Default Editor

Demo 5m:

When you make a commit and don't specify a commit message using `-m`,
Git opens a plain text editor so you can write a commit message. But
which editor opens depends on how it is configured.

1. First, let's figure out how Git determines which editor to use. Use
    the following command to get help on the variables that Git uses.

    ```bash
    git help var
    ```

2. Find the variable that git uses to determine which editor to use.
    Past it and its description below.


3. Let's inspect these sources in the order that Git looks for them.
    Like Git, stop when you get a non-empty value.

    ```bash
    echo "$GIT_EDITOR"
    ```

    If that's empty, try...

    ```bash
    git config --global core.editor
    ```

    If that's empty, try...

    ```bash
    echo "$VISUAL"
    ```

    If that's empty, try...

    ```bash
    echo "$EDITOR"
    ```

    It that's empty, it depends on the build of Git you're using. But
    it will probably be `vi`.

4. Try changing your Git editor to `nano`. Do this by either setting
    an environment variable...

    ```bash
    VISUAL=nano
    ```

    Or by setting core.editor in Git

    ```bash
    git config --global core.editor
    ```

5. Test your change by making a commit.

    ```bash
    touch a
    git add a
    git commit      # no message with -m, so the editor will open.
    ```

Feel free to change it back.

This shows that some Git configuration is complicated and may require
adjustments to your OS/environment and or Git configuration.

### Model 5 - `git pull --ff-only`

5m

`git pull` tries to pull changes from another repository into your local
repository. This is safe when you don't have any new changes locally. Then
you are simply updating your local repository with those in the other.

But what if you have made changes in your local repository. And what if
someone has made new changes in the other repository. Effectively both
of you made changes "at the same time". Then when you try to pull, git
has to know how you want to deal with this situation.

We'll learn how to do this. But for now, we want to prevent git from automatically
taking any actions that would require us to possibly have to undo those
actions. In other words, if git detects such a situation, we want it to
stop, tell us there is a problem, and then we can decide how we want to
handle it. This makes `git pull` a safe operation. Let's set this as the
default using `pull.ff`. `ff` here stands for "fast-forward".


1. `pull.ff` is the configuration we need to set. Give your command.
    What is it currently set to?

2. Set it to `only`. Give your command.


## Model 6

10m

Help your teammates to complete these above configuration on their computer.

## Summary

Git is highly configurable, but the good news is that the defaults are usually what you want.
The configuration described above may be all you need to set.
Again, to learn more about what configurations are possible,
see `git help config` or <https://git-scm.com/docs/git-config> .

## Bonus: Public Lab

If you must work in a public lab and need to set these configurations regularly,
you can create a script to automate the process. This is one of the strengths of
the command-line interface, they are automate-able!

Create a plain-text file named `configure-git.bash` with the following contents
with "Your Name" and "your.email@someservice.com" replaced with your information:

```bash
# File: configure-git.bash
git config --global user.name "Your Name"
git config --global user.email "your.email@someservice.com"
git config --global init.defaultbranch main
git config --global core.editor "code --wait"
```

Make this file executable:

```bash
chmod +x configure-git.bash
```

Now copy this file to a flash drive and carry it with you to the lab. When you sit down, in the lab, open Git-Bash and run the following, adjusting the drive letter appropriately.

```bash
bash /e/configure-git.bash
```

You could also create another script to cleanup before you leave.

```bash
# File: unconfigure-git.bash
git config --global user.name "Bogus name"
git config --global user.email "bogus.email@someservice.com"
```

Make it executable...

```bash
chmod +x unconfigure-git.bash
```

And copy it to your flash drive.

## Bonus: GitPod

If you use GitPod, figure out how you can set the above options so that
they persist for each workspace you create.


--------------------------
Copyright 2021, Darci Burdge and Stoney Jackson

This work is licensed under the Creative Commons Attribution-ShareAlike
4.0 International License. To view a copy of this license, visit
<http://creativecommons.org/licenses/by-sa/4.0/> .
