# GitKit

After the first two-weeks of class, you will have completed a sequence
of 4 activities to learn a common workflow to contribute to projects
on services like GitHub, GitLab, and BitBucket.

In the GitKit, you learned:

1. Principles, roles, and terminology in open source communities and projects.
2. How to fork a project and identify if a project is a fork or the upstream.
3. How to open your fork in GitPod, which clones the repository and creates
    an origin and upstream remote.
4. How to use an issue tracker to find an issue to work on, and coordinate
    effort by claiming an issue before you work on it.
5. How to create a new feature branch, and make it the active branch.
6. How to make, stage, and commit a change to a branch.
7. How to publish a branch to your fork and create a pull-request back to upstream.
8. How to update your pull-request by adding more commits to your feature branch.
9. How to synchronize your fork's main branch from upstream's.
10. How to update your feature branch with new changes in main and resolve
    any conflicts when you do.

To study for the exam, review the slides and the worksheets you submitted
for each of these four sections.