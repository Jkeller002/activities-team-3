# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter:
* Recorder & Pilot:
* Quality Control:
* Efficiency Expert:

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea? 

No, you can not copyright an idea unless it is written down or executed.

2. You have written a new program based on that idea, is that software copyright-able?

Yes.

3. You have written a new program, what do you have to do to copyright that program?

Yes/no, its automatically copyrighted but can be registered to be enforced.

4. If you own the copyrights to something, what can you do that no one else can do? (see <http://www.bitlaw.com/copyright/scope.html> ) 

The right's to reproduce, prepare derivative works, distribute copies, perform, and display.

5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?

Nothing, legally.

6. What is the purpose of a license?

The puropse of a License is to Guarantee user freedom, prevent developers from restricting or locking-in users. They also prevent the building of propieraty software. Permissions / obligations. 

7. When you license your program do you give up your copyright?

No.

8. As the copyright holder, once you license your program, could you later license it under a different license?

Yes.

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license?

Yes

10. Can you sell open-source licensed software?

No, its free since its open software.

11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?

Company

12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?

You and Your friend. Both must be in agreement and obtain a copyright notice

13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?

Get coptright disclaimer


14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core> - Mozilla Public License, version 2.0
    2. <https://github.com/apache/fineract> -  Apache License Version 2.0, January 2004
    3. Find VSCodium on GitHub. - MIT License

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.

Mozilla Public License 2.0
Cans - Modify, Distribute, Sublicense, Plan Warranty, Use Patent Claims, Commercial Use
Cannot - Use Trademark, Hold liable
Must - Include Copyright, Include License, Disclose Source, Include Original

Apache License 2.0
Cans - Commercial Use, Modify, Distribute, Sublicense, Place Warranty, Private Use, Use Patent Claims
Cannot - Hold Liable
Must - Use Trademark 

MIT License
Cans -  Commercial Use, Modify, Distribute, Sublicense,  Private Use
Cannot - Hold Liable
Must - Include Copyright, Include License

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?

Copyleft licenses are persistant wheras permisive licenses are not.

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
