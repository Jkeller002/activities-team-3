# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: I
* Recorder: BC
* Spokesperson: BC
* Researcher: TB


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition, Theory, and Values

Time: 25m

Read the section `Scrum Definition` and answer the following questions.

1. True or false; you can apply Scrum as described in The Scrum Guide.
A: True
2. True or false; you should not deviate from Scrum as described in The Scrum Guide.
A: False
3. True or false; Scrum is a completely defined process for software development.
A: False

Read the section `Scrum Theory` and answer the following questions.

1. What are the three pillars of Scrum?
A: transparency, inspection, adaptation
2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?
A: artifacts that have low transparency can lead to decisions that diminish value and increase risk
3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?
A: bugs and other issues that go unnoticed
4. What is necessary for "adaptation" to be possible?
A: inspection enables adaptation

Read the section `Scrum Values` and answer the following questions.

1: What values does Scrum add to the Agile values?
A: Commitment, Focus, Openness, Respect, and Courage
2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 4: Scrum Team

Time: 20m

Read the section `Scrum Team` and answer the following questions.


1. What roles are there in the Scrum Team?
A: one Scrum master, one Product Owner, and Developers
2. How big should a Scrum Team be?
10 or fewer people
3. Which of these teams is cross-functional?
   1. A database team that is able to design and implement the database.
   yes
   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert.
   yes
4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog.
   A: Developers
   2. Clearly communicates items in the Product Backlog.
A: Product Owner
   3. Ensures Scrum events take place and are positive, productive, and time-boxed.
A: Scrum Master
   4. Orders/prioritizes the items in the Product Backlog.
A: Product Owner
   5. Removes barriers between stakeholders and the Scrum Team.
   A: Scrum Master
   6. Holds the team accountable as professionals.
A: Developers
   7. Adhering to the Definition of Done.
A: Developers
   8. Adapting the plan each day toward the Sprint Goal.
A: Developers
   9. Clearly communicates the Product Goal.
A: Product Owner

---

> ***STOP*** Review as a class before continuing.

Time: 10m

---

## Model 5: Scrum Events

Read the section `Scrum Events` and answer the following questions.

Time: 30m

Scrum Events

1. What are the major Scrum events?
A: Sprint, Planning, Daily Scrum, Sprint Review, Restrospective
2. What is the fundamental purpose of all Scrum events?
A: Each even in scrum is a formal opportunity to inspect and adapt strong artifacts

Sprints

3. How long is a typical Sprint?
A: 1 month or less
4. True or false; the length of a Sprint is determined by the amount of work that needs to be done?
A: False
5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint?
A: False
6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent the Team from meeting the Spring Goal on time. Which of the following is allowed by Scrum?
A: 3rd piece
   1. Renegotiate the end of the current Sprint?
   2. Renegotiate the quality of the work being done in the current Sprint?
   3. Renegotiate the scope of the work of the current Sprint?
7. When may a Sprint be canceled and by whom?
A: A sprint can be canceled if the sprint goal becomes obselete and only the product owner can cancel
Sprint Planning

1. Purpose?
A: It initates the sprint to be laid out by the sprint
2. Length?
A: 8 hours max for one month sprint
3. Frequency?
A: once per sprint
4. When during the Sprint?
A: At the start
5. Who are involved?
A: The product owner, the scrum team and others
6. What is the input?

7. What is the output?

Daily Scrum

1. Purpose?
A: inspect progress toward the Sprint Goal and adapt the Sprint Backlog as necessary, adjusting the upcoming planned work.
2. Length?
15 minutes
3. Frequency?
A: Once a day
4. When during the Sprint?
A: same time and place every working day of the Sprint.
5. Who are involved?
A: Developers of the scrum team or Product Owners and Scrum Masters if they are actively working on items in the Sprint Backlog.
6. What is the input?
A: Talk about day to day changes to make to improve for the next day.
7. What is the output?
A: Daily Scrums improve communications, identify impediments, promote quick decision-making, and consequently eliminate the need for other meetings.

Sprint Review

1. Purpose?
A: inspect the outcome of the Sprint and determine future adaptations.
2. Length?
A: timeboxed to a maximum of four hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.
3. Frequency?
once
4. When during the Sprint?
A: the second to last event
5. Who are involved?
Scrum Team and Stakeholders
6. What is the input?
A: the Scrum Team and stakeholders review what was accomplished in the Sprint and what has changed in their environment.
7. What is the output?
A: Based on this information, attendees collaborate on what to do next

Sprint Retrospective

1. Purpose?
A: plan ways to increase quality and effectiveness.
2. Length?
A: It is timeboxed to a maximum of three hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.
3. Frequency?
A: Once
4. When during the Sprint?
A: The Sprint Retrospective concludes the Sprint. 
5. Who are involved?
The Scrum Team
6. What is the input?
A: identifies the most helpful changes to improve its effectiveness.
7. What is the output?
A: The most impactful improvements are addressed as soon as possible. They may even be added to the Sprint Backlog for the next Sprint.
---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 6: Scrum Artifacts

Time: 30m

Read the section `Scrum Artifacts` and its subsections and answer the following questions.

Product Backlog

1. What is it and what does it contain?
A: Product Backlog is an emergent, ordered list of what is needed to improve a product. Single source of work undertaken by Scrum Team
2. Who is responsible for sizing an item in the backlog?
A: Developers
3. Who is responsible for ordering the items in the backlog (highest priority first)?
A: Product Owner
4. What is the Product Goal and its purpose?

A: Product Goals describe a future state of the prouct which can serve as a target for the Scrum Team.
Sprint Backlog

1. What is in the Sprint Backlog?
A: The Sprint Backlog is a plan by and for the Developers. It is a highly visible, real-time picture of the work that the Developers plan to accomplish during the Sprint in order to achieve the Sprint Goal.
2. What is the Sprint Goal and its purpose?

A: The Sprint Goal is the single objective for the Sprint. The Sprint Goal is created during the Sprint Planning event and then added to the Sprint Backlog. As the Developers work during the Sprint, they keep the Sprint Goal in mind
Increment
1. What is it?
A: It is a concrete stepping stone toward the Product Goal
2. How many may be produced as part of a sprint?
A: Multiple can be created within a sprint
3. What is the Definition of Done?
A: It is a formal description of the state of the Increment when it means the quality measures required for the product.
4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?
A: It cannot be released or even presented at the Sprint Review, and it returns to the Product Backlog for future consideration

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
