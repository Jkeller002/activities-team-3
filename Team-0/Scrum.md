# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: T.T
* Recorder: T.T
* Spokesperson: D.N.
* Researcher: D.N.


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition, Theory, and Values

Time: 25m

Read the section `Scrum Definition` and answer the following questions.

1. True or false; you can apply Scrum as described in The Scrum Guide.
  - True
2. True or false; you should not deviate from Scrum as described in The Scrum Guide.
  - False, within reason.
 - There are key pieces that should be included for it to be considered Scrum.
3. True or false; Scrum is a completely defined process for software development.
- False

Read the section `Scrum Theory` and answer the following questions.

1. What are the three pillars of Scrum?
   - Transparency, inspection, and adaptation.
2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?
   - Transparency enables inspection and inspection without transparency is misleading and wasteful.
   - Without transparency there is nothing to inspect.
3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?
   - Inspection enables adaptation. Inspection without adaptatin are pointless.
   - Without inspecting there is nothing to adapt.
4. What is necessary for "adaptation" to be possible?
- People being involved and self-managing.

Read the section `Scrum Values` and answer the following questions.

1. What values does Scrum add to the Agile values?
- Commitment, Focus, Openness, Respect, and Courage.
- ^ all goes back to empowering the team
2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 4: Scrum Team

Time: 20m

Read the section `Scrum Team` and answer the following questions.


1. What roles are there in the Scrum Team?
- Scrum Master, Product Owner, and Developers
2. How big should a Scrum Team be?
- 10 or fewer people.
3. Which of these teams is cross-functional?
   1. A database team that is able to design and implement the database.
   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert.
   - 2 is cross funcitional where 1 is not.
4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog.
   - Developers
   2. Clearly communicates items in the Product Backlog.
   - Product owner
   3. Ensures Scrum events take place and are positive, productive, and time-boxed.
   - Scrum master
   4. Orders/prioritizes the items in the Product Backlog.
      - Product owner
   5. Removes barriers between stakeholders and the Scrum Team.
      - Scrum master
   6. Holds the team accountable as professionals.
      - Developers
   7. Adhering to the Definition of Done.
      - Developers
   8. Adapting the plan each day toward the Sprint Goal.
      - Developers
   9. Clearly communicates the Product Goal.
      - Product Owner

---

> ***STOP*** Review as a class before continuing.

Time: 10m

---

## Model 5: Scrum Events

Read the section `Scrum Events` and answer the following questions.

Time: 30m

Scrum Events

1. What are the major Scrum events?
   - The Sprint, Spring Planning, Daily Scrum, Sprint Review, Sprint Retrospective.
2. What is the fundamental purpose of all Scrum events?
   - To inspect and adapt.
   - To create regularatory and to reduce complexity.

Sprints

3. How long is a typical Sprint?
   - One month or less.
4. True or false; the length of a Sprint is determined by the amount of work that needs to be done?
   - False
5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint?
   - True
6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent the Team from meeting the Spring Goal on time. Which of the following is allowed by Scrum?
   1. Renegotiate the end of the current Sprint?
   2. Renegotiate the quality of the work being done in the current Sprint?
   3. Renegotiate the scope of the work of the current Sprint?
   - 3 is allowed by Scrum.
7. When may a Sprint be canceled and by whom?
   - If the goal becomes obsolete, and only by the product owner.

Sprint Planning

1. Purpose?
   - To plan out the sprint.
   - What needs to get done, how it will get done.
2. Length?
   - A maximum of eight hours.
3. Frequency?
   - Anytime a sprint is necessary.
4. When during the Sprint?
   - Before the Sprint.
5. Who are involved?
   - All.
6. What is the input?
   - Sprint goal
7. What is the output?
   - Sprint backlog

Daily Scrum

1. Purpose?
   - To inspect progress towards the Sprint goal.
2. Length?
   - 15 minutes.
3. Frequency?
   - Same time and place every work meeting of the sprint.
4. When during the Sprint?
   - Before Sprint review.
5. Who are involved? The developers
6. What is the input?
   - Sprint Goal
7. What is the output?
   - Improving communications, identifying challenges, and promoting 
   quick decision making.

Sprint Review

1. Purpose?
   - To inspect the outcome of the Sprint and determine
   future adaptations.
2. Length?
   - Maximum 4 hours for a one month sprint.
3. Frequency?
   - Once
4. When during the Sprint?
   - Second to last event of the Sprint.
5. Who are involved?
   - Scrum team and stakeholders involved.
6. What is the input?
   - The Sprint.
7. What is the output?
   - Review of the sprint and possible adaptations.

Sprint Retrospective

1. Purpose?
   - Plan ways to increase quality and effectiveness.
2. Length?
   - Maximum of 3 hours for a one month sprint.
3. Frequency?
   - Once.
4. When during the Sprint?
   - Last event of sprint.
5. Who are involved?
   - Scrum team; but mainly developers.
6. What is the input?
   - Identifying most impactful improvements.
7. What is the output?
   - Adding it to the Scrum backlog for the next Sprint.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 6: Scrum Artifacts

Time: 30m

Read the section `Scrum Artifacts` and its subsections and answer the following questions.

Product Backlog

1. What is it and what does it contain?
   - Its a list of whats needed to improve the product.
2. Who is responsible for sizing an item in the backlog?
   - The developers.
3. Who is responsible for ordering the items in the backlog (highest priority first)?
   - Product owners.
4. What is the Product Goal and its purpose?
   - Describes a future state of the product and can serve as a target for the Scrum team.

Sprint Backlog

1. What is in the Sprint Backlog?
   - Sprint Goal, set of product backlog items, and a plan for delivering the increments.
2. What is the Sprint Goal and its purpose?
   - The single objective for the Sprint to create coherence and focus.

Increment
1. What is it?
   - A concrete stepping stone towards the product goal.
2. How many may be produced as part of a sprint?
   - Multiple.
3. What is the Definition of Done?
   - A formal description of the state of the increment when it meets the quality measures required for the product.
   - Requirements for the increment to be done.
4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?
   - It returns to the product backlog for future consideration.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
