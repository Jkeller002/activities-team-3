# Linux Shell Commands - Activity

The command-line allows us to execute commands using a text-based
interface. We'll be using the command-line to navigate file-systems,
manipulate files and directories, and interacting with git.

This activity introduces some basic Linux commands for navigating a
file-system and manipulate its files and directories. The commands
presented in this activity should work in most Linux shells. Many of
them will also work in PowerShell for Windows. For the purposes of this
activity, we'll assume a Bash-like shell.

> **Why Bash? Why Linux? Why open-source?**
>
> Open-source is everywhere. Linux is an open-source operating system,
> which has long been used to run most servers and is now the foundation
> for many personal devices (e.g., Android), set-top boxes (e.g., TiVo),
> and many smart devices (e.g., Ecobee), and serves as the foundation
> of most cloud-based computing platforms (e.g., AWS and Google Cloud).
> Big companies like Microsoft and Google are increasingly using and
> contributing to open-source software, recognizing its importance to
> their business model (e.g., Microsoft purchased GitHub, VS Code is
> made by Microsoft and it's open-source, IBM purchased RedHat, etc.).
> So, as a software developer, you need to be familiar with open-source
> and its ecosystem of tools. That means Linux, Linux-based shells,
> Linux commands, etc. Also, git itself is an open-source version control
> tool invented by the same person who invented Linux, Linus Torvalds.

## Roles

* Manager: Dylan N
* Driver: Dylan N
* Recorder: Toby T
* Spokesperson: Naz H


## Model 1 - Terminology

Lecture: 5m

A **terminal** is a program that runs a **shell**. A terminal used to be a physical machine, but now it is a program like Terminal.app, iTerm, etc. Basically, the terminal allows you to type text with the keyboard and view text on your screen.

A **shell** is a program that interprets and executes command-line commands: e.g., Bash, Zsh, DOS, and PowerShell. These shells are essentially programming languages that specialize in manipulating an operating system and its filesystem. DOS, for example, stands for Disk Operating System. Each command you type into a shell is interpreted and executed, and its results are printed to the terminal.

In some cases these terms are used interchangeably.

## Model 2 - Starting a Linux-based Shell

By default, GitPod provides a bash shell.

Have your driver/pilot open your team's repository in GitPod.

The changes your driver makes will not be made permanent. So they
will not commit and push their changes.

Only your recorder will commit their changes at the end of today's session.

## Model 3 - Linux Commands

Intro: 5m

> Square brackets indicate optional arguments.

* `pwd`
  * Print the absolute path to the current working directory.
* `ls [-a]`
  * Display the contents of the current working directory.
  * `-a` displays all files, including hidden files.
* `cd` *[path]*
  * Change the current working directory to the provided path. If no path is provided, change to the current user's home directory. If the path is absolute, change to that directory. If the path is relative, change to that directory relative to the current directory. 
* `mkdir` [`-p`] *path*
  * Create the new directory given at the end of path. If `-p` is provided, creates all directories given in the path.
* `touch` *path*
  * Update the last-modified time of the file or directory named by path, or create a new file if the named file doesn't exist.
* `rm` [`-r`] *path*
  * Remove the file named by path. `-r` allows you to delete a directory and all its contents recursively.

> !! Be careful with `rm` and `rm -r`. There is no "trashcan" in linux. If you delete something, it's gone.

* `cat` *path* [...]
  * Display the contents of each file.
* `less` *path*
  * Display the contents of a file using a pager. Press `b` to go back a page, `spacebar` to go forward a page, `/pattern` to search for pattern and `n` to find the next occurrence, and `q` to quit.
  * This pager is used in some `git` commands and `man`.
* `man` *command*
  * View the documentation for *command*.
  * Uses the `less` pager.
* `info` *command*
  * Use this if `man` doesn't return anything useful because the command is a builtin command.

### Instructions

Time: 10m

Complete the instructions below as follows:

* Recorder: Read out each instruction.
* Driver: Perform the instruction.
* All: Help your driver complete the instruction. Observe and discuss the result.
* Recorder: Record the answer.

Have your driver use the commands above to answer the following questions.
Recorder:

1. Give the full path to your current working directory.
    What command did you use? \workspace\activities
      pwd command
2. How many (non-hidden) files or directories are in your current directory. 12 files/directories
    What command did you use? ls
3. What hidden files are in your current directory. Name them. 1
    What command did you use? ls -a
4. Change your current directory to a subdirectory of the current directory.
   (i.e., pick a directory and move into it.).
  - What is the absolute path to that directory? \Home\gitpod\go-packages
  - How many files does it contain? 3 files
5. Use a command-line command to create a file named `foods.txt`.
    What command did you use? mkdir foods.txt
6. Edit the file using your IDEs editor and your team's 3 favorite foods.
7. Use a command-line command to display the contents of `foods.txt`.
    What command did you use? cat foods.txt
8. Change your current directory to be the parent of the current directory.
    (i.e., move to the parent directory). 
    What command did you use? cd
9. Delete the `foods.txt` file. Remember it's in a subdirectory.
    What command did you use? rm -r
10. Create a directory named `gnat`.
    What command did you use? mkdir 'gnat'
11. Delete the `gnat` directory.
    What command did you use? rm 'gnat'

## Model 5 - Tab Completion and the History

Demo: 5m

It's hard to remember the details of every command and it is easy to mistype a command. Using a shell's tab-completion and a history features can help.

* Tab-completions - Start to type the name of a command, directory, file, etc., and then press tab. If what you typed is enough to uniquely identify the thing you are trying to type, the shell will complete it for you. If not, you'll probably get some beep. Press tab again and the shell may show some possible completions and allow you to select from them. This is handy to avoid typos and helps you remember the exact name of something.
* History - use the up and down arrows to traverse the history of commands you've recently typed. When you find the one you are looking for, press enter to execute it again, or edit it first and then press enter.
* History-completion - I made up this term. But some shells will let you type a little on the command-line, and then press the up arrow to search through your history filtering by what you just typed. For example, if you know you are looking for a recent `git` command -- type `git` then use the up and down arrows to walk through your history of `git` commands.

### Instructions

Have your driver try each of the above.

Time: 5m

## Clean up

Driver: stop your GitPod workspace.

Recorder: commit and push your team's answer to your team's fork, and issue
an MR to upstream.

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2021, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
