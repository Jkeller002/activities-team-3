# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter: 
* Recorder & Pilot:
* Quality Control:
* Efficiency Expert:

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea? No you can't copyright an idea.

2. You have written a new program based on that idea, is that software copyright-able? Yes

3. You have written a new program, what do you have to do to copyright that program? Use a license

4. If you own the copyrights to something, what can you do that no one else can do? (see <http://www.bitlaw.com/copyright/scope.html> ) Reproduce, prepare derivative works, distribute copies, perform and display work publicly.

5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it? Whatever you want, there is nothing to stop you.

6. What is the purpose of a license? Set rules on he code like who can use it, and what they can do to the code.

7. When you license your program do you give up your copyright? No

8. As the copyright holder, once you license your program, could you later license it under a different license? Yes

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license? No, because your program is under a new license.

10. Can you sell open-source licensed software? No, because it is free.

11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license? The company owns it, ad whatever the company decides the license is, it will be.

12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do? You both own the copyright and add a .license file with the MIT license in it

13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under? Add a .license file with the GPL-v3.0 license in it. You still own the copyright, and their code is licensed under the same license.

14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core> Modzilla Public License v2.0
    2. <https://github.com/apache/fineract> Apache License v2.0
    3. Find VSCodium on GitHub. MIT License

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.

Modzilla Public 2.0
- CAN: commercial use, modify, distribute, sublicense, place warranty, use patent claims
- CANT: Use trademark, Hold Liable
- MUST: Include copyright, include licence, disclose source, include origional
Apache 2.0:
- CAN: commercial use, modify, distribute, sublicence, place warranty, lrivate use, use patent claims
- CANNOT: Hold liable, use trademarks
- MUST: include copyrigt, inlcude license, state changes, inlcude notice
MIT License
- CAN: commerical use, modify, distribute, sublicense, private use
- CANT: hold liable
- MUST: inlcude copyrigt, include licence

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license? A permissive open source license allows for the user and contributor to have more freedom, whereas the non-permissive one doesn't allow as much.

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
