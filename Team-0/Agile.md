# Agile

## Roles

* Manager: Dylan
* Recorder: Toby/Naz
* Spokesperson:
* Researcher: Dylan


## Model 1 - What is Agile?

12m Video: *What is Agile?* by Mark Shead. May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

Questions: 5m

1. Is Agile a process?
Its value allows and principles 
2. Is Agile a framework?
Yes
3. Is Agile a set of tools?
No its not
4. What is Agile?
Agile is a framework of steps that will help get your project done 
5. What does Agile help companies and developers do?
Helps them complete projects correctly, and quickly
Class discussion: 5m


## Model 2 - The Agile Manifesto

Time: 10m

At the core of The Agile Manifesto are four value statements.
They are structured as follows:

```
We value

    ___`Individuals`_________  ___and interactions`_________  over  ____proccess________  ____and tools________

    _____Working_______  _____software_______  over  _____comprehensive_______  ___documentation_________

    ________Customer____  ___collaboration_________  over  ____contract________  ____negotiation________

    _____Responding_______  _to change___________  over  ____following________  _a plan___________

```
The missing phrases are mixed up below. Unscramble them to reconstruct the
value statements of the Agile Manifesto. Try to do it without looking up the Manifesto.

* `Individuals`
* `Working`
* `Responding`
* `Customer`
* `documentation`
* `and tools`
* `negotiation`
* `collaboration`
* `following`
* `and interactions`
* `software`
* `comprehensive`
* `proccess`
* `to change`
* `a plan`
* `contract`

Once you are satisfied, check your work against [The Agile Manifesto](https://www.agilealliance.org/agile101/the-agile-manifesto/) . How many statements did your team get right (ignoring the order of the statements)?

1. Does the Manifesto imply that projects should not use tools or follow a process?
It implies that you should focus on the individuals around you and interact with them rather than the process and tools
2. Does the Manifesto imply that projects should not write any documentation?
It doesn't want you to document every single thing
3. Does the Manifesto imply that projects should not have contracts with clients?
Its implying that is rather customer collboration over contracts.
4. Does the Manifesto imply that projects should not create or follow a plan?
No it says that you have to be able to respond to change
5. Based on their values, why would an Agile team reject the waterfall method to developing software?
Because they have a more effective way of doing things

Class discussion: 10m


## Model 3

Time: 20m

Below are the 12 Agile principles that embody the 4 Agile value statements.
For each, identify which value statement the principle supports or is most related to,
and be prepared to explain how.

1. Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
  - Working software over comprehensive  documentation.

2. Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
- Responding to change over following a plan.

3. Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
- Working software over comprehensive documentation.

4. Business people and developers must work together daily throughout the project.
- Individuals and interactions over processes and tools.

5. Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
- Individuals and interactions over processes and tools.

6. The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
- individuals and interactions

7. Working software is the primary measure of progress.
- working software over comprehensive documentation.

8. Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
- Customer collaboration
9. Continuous attention to technical excellence and good design enhances agility.
- Working software over comprehensive documentation.

10. Simplicity--the art of maximizing the amount of work not done--is essential.
- Responding to change

11. The best architectures, requirements, and designs emerge from self-organizing teams.
- Individuals and interactions.

12. At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.
- Responding to change.

Class discussion: 20m


## References

* 12m Video: *What is Agile?* by Mark Sheed May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

* The Agile Manifesto

  * Values

    * Original: <https://agilemanifesto.org/>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/the-agile-manifesto/>

  * Principles

    * Original: <https://agilemanifesto.org/principles.html>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/>


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
