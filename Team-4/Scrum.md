# Scrum

We will learn about the roles, artifacts, and the Sprint Cycle used in the Scrum development framework.


## Content Learning Objectives

_After completing this activity, students should be able to:_

*   List the Scrum roles, and describe their part in the development process.
*   List the Scrum artifacts, and describe their part in the development process.
*   List the parts of the Sprint Cycle, and describe how it is used in the development process.


## Process Skill Goals


_During the activity, students should make progress toward:_

*   Carefully reading a text for understanding. (Information Processing)


## Roles

* Manager: RS
* Recorder: LG
* Spokesperson: BL
* Researcher: BL


Answer the questions in this activity by referring to [The Scrum Guide](https://www.scrumguides.org/scrum-guide.html) by Ken Schwaber and Jeff Sutherland. 2020.


## Model 1: Scrum Definition, Theory, and Values

Time: 25m

Read the section `Scrum Definition` and answer the following questions.

1. True or false; you can apply Scrum as described in The Scrum Guide.
True
2. True or false; you should not deviate from Scrum as described in The Scrum Guide.
False
3. True or false; Scrum is a completely defined process for software development.
False

Read the section `Scrum Theory` and answer the following questions.

1. What are the three pillars of Scrum?
Transparency, Inspection, Adaptaion
2. Imagine "transparency" was not a pillar. How would this impact the remaining two pillars?
This would impact the remaining two pillars because without transparency, you would have misleading inspection, which also impact adaptation.
3. Imagine "inspection" was not a pillar. How would this impact the remaining two pillars?
This would impact the remaining two pillars because inspection enables adaptaion and prevents frequent changes.
4. What is necessary for "adaptation" to be possible?
It is necessary to be involved and self-managed for adaptation to be possible.

Read the section `Scrum Values` and answer the following questions.

1. What values does Scrum add to the Agile values?
The values that Scrum adds to the Agile values are Commitment, Focus, Openness, Respect, and Courage
2. Read out loud the paragraph that explains these values and how they relate to each other, the Scrum Team, stakeholders, and the Sprint.
These values relate to each other with how they allow each member of a team to support and encourage each other. For the Scrum Team, the small team allows everyone to get to know and talk to each other. For stakeholders, it allows for openness and communication with one another. Lastly for the Sprint, everyone on the team are trying to reach the same goal and are determined to reach it and support each along the way.
---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 4: Scrum Team

Time: 20m

Read the section `Scrum Team` and answer the following questions.


1. What roles are there in the Scrum Team?
The roles that are in the Scrum Team are one Scrum Master, one Product Owner, and a group of Developers.

2. How big should a Scrum Team be?
The Scrum Team's size should consist of ten or fewer people.

3. Which of these teams is cross-functional?
   1. A database team that is able to design and implement the database.
   This team is cross-functioncal because Scrum Teams are cross-functional.

   2. A team with a database expert, a test expert, a human-computer interaction expert, a front end expert, a backend expert, and a documentation expert.
   This team is cross-functional because Scrum Teams are cross-functional.

4. For each responsibility below write which role is accountable for that responsibility, or write "all" if the whole team is accountable.
   1. Create a plan for the Sprint in the Sprint Backlog.
   Developers

   2. Clearly communicates items in the Product Backlog.
   Product Owner

   3. Ensures Scrum events take place and are positive, productive, and time-boxed.
   Scrum Master

   4. Orders/prioritizes the items in the Product Backlog.
   Product Owner

   5. Removes barriers between stakeholders and the Scrum Team.
   Scrum Master

   6. Holds the team accountable as professionals.
   Developers

   7. Adhering to the Definition of Done.
   Developers

   8. Adapting the plan each day toward the Sprint Goal.
   Developers

   9. Clearly communicates the Product Goal.
   Product Owner

---

> ***STOP*** Review as a class before continuing.

Time: 10m

---

## Model 5: Scrum Events

Read the section `Scrum Events` and answer the following questions.

Time: 30m

Scrum Events

1. What are the major Scrum events?
The major Scrum events are The Sprint, Sprint Planning, Daily Scrum, Sprint Review, and Sprint Retrospective.

2. What is the fundamental purpose of all Scrum events?
The fundamental purpose of all Scrum events is to complete and review Sprints to make improvements and changes to future ones.

Sprints

3. How long is a typical Sprint?
A typical Sprint is typically one month or less.

4. True or false; the length of a Sprint is determined by the amount of work that needs to be done?
True

5. True or false; a Sprint only consists of the time that work is being done. No meetings may take place during a Sprint?
False

6. Suppose the Team learns that one of the items in the Sprint Backlog is going to take more time than originally anticipated, and would likely prevent the Team from meeting the Sprint Goal on time. Which of the following is allowed by Scrum?
   1. Renegotiate the end of the current Sprint?
   This is not allowed.

   2. Renegotiate the quality of the work being done in the current Sprint?
   This is not allowed.

   3. Renegotiate the scope of the work of the current Sprint?
   This is allowed.

7. When may a Sprint be canceled and by whom?
The Sprint may be cancelled anytime by the Product Owner if the Sprint Goal becomes obsolete.

Sprint Planning

1. Purpose?
Establish an obtainable goal.

2. Length?
Eight-hours for a one month Sprint. This time is shorter for shorter Sprints.

3. Frequency?
Once per Sprint.

4. When during the Sprint?
This is the first step. This happens before the Sprint occurs.

5. Who are involved?
The entire team is involved (10 or fewer).

6. What is the input?
The input is the work layed out to be performed for the Sprint.

7. What is the output?
The output is the resulting plan for the Sprint.

Daily Scrum

1. Purpose?
To inspect progress towards the Sprint Goal and to adapt the Sprint Backlog as necessary.

2. Length?
Fifteen minutes.

3. Frequency?
Every day.

4. When during the Sprint?
Continuous throughtout the Sprint and at the same time and place every working day of the Sprint.

5. Who are involved?
Developers.

6. What is the input?
The input is updates on the progress towards the Sprint Goal.

7. What is the output?
The output is improved communications, identified impediments, quicker decision-making, and the elimination for the need of other meetings. Additionally, it produces an actionable plan for the next day of work.

Sprint Review

1. Purpose?
To inspect the outcome of the Sprint and to determine future adaptations.

2. Length?
Maximum of four hours for a one-month Sprint. For shorter Sprints, this time is shorter.

3. Frequency?
Once after the Sprint is over.

4. When during the Sprint?
Second-to-last step. This happens after the Sprint.

5. Who are involved?
The Scrum Team and stakeholders are involved.

6. What is the input?
The input are the accomplishments and what has changed in their environment as a result of the Sprint.

7. What is the output?
The output is future adaptions of the Sprint.

Sprint Retrospective

1. Purpose?
To plan ways to increase quality and effectiveness.

2. Length?
Maximum of three hours for one-month Sprints. For shorter Sprints, this time is shorter.

3. Frequency?
Once after Sprint Review.

4. When during the Sprint?
This is the last step. This happens after the Sprint Review.

5. Who are involved?
The Scrum Team is involved.

6. What is the input?
The input is an evalutation of how the Sprint went in regards to individuals, interactions, processes, tools, and the Definition of Done.

7. What is the output?
The output is an identification of the most helpful changes to improve the Sprint's effectiveness.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

## Model 6: Scrum Artifacts

Time: 30m

Read the section `Scrum Artifacts` and its subsections and answer the following questions.

Product Backlog

1. What is it and what does it contain?
This is an ordered list of what is needed to improve product. It contains items that can be Done by the Scrum Team.

2. Who is responsible for sizing an item in the backlog?
The Developers are responsible for sizing an item in the backlog, but they may receive influence from the Product Owner by helping them understand and select trade-offs.

3. Who is responsible for ordering the items in the backlog (highest priority first)?
1) Developers
2) Product Owner

4. What is the Product Goal and its purpose?
This describes a future state of the product. Its purpose is to serve as a target for the Scrum Team to plan against.

Sprint Backlog

1. What is in the Sprint Backlog?
The Sprint Goal, the set of Product Backlog items selected for the Sprint, and the actionable plan for delivering the increment.

2. What is the Sprint Goal and its purpose?
This is the single objective for the purpose. Its purpose is to provide flexibility in terms of the exact work needed to achieve this objective.

Increment
1. What is it?
This is a concrete stepping stone toward the Product Goal.

2. How many may be produced as part of a sprint?
There may be multiple that may be created as part of a Sprint.

3. What is the Definition of Done?
The Definition of Done is a formal description of the state of the increment when it meets the quality measures required for the product.

4. What happens to a work item that does not meet the Definition of Done by the end of a Sprint?
The work item cannot be released nor presented during the Sprint Review and instead it returns to the Product Backlog for future consideration.

---

> ***STOP*** Review as a class before continuing.

Time: 15m

---

<hr>
Copyright © 2019 Karl R. Wurst and Stoney Jackson. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
