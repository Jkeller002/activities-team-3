# Markdown Activity

Time: 15m

## Roles

- Manager: Ryan Santos
- Recorder: Lucas Garcia
- Spokesperson: Benjamin Lord

## Instructions

As a team, use your favorite search engine and find the answers to the following questions.

1. What is Markdown?
Markdown is a markup language that describes how text should look on a page.
2. What is CommonMark?
CommonMark is an alternative version of Markdown in attempt to remove inconsistencies of the original Markdown.
3. What is GitHub Flavored Markdown?
GitHub Flavored Markdown is a dialect of Markdown utilized by GitHub.
4. What kind of file do you use to create Markdown?
The kind of file that you use to create Markdown is a .md or .markdown file.
5. In Markdown How do you create
    1. A heading?
    Add a # in front of a word or phrase.
    2. A list?
    For an ordered list, number an item followed by a period. For a nonordered list, add a -, *, or a +.
    3. A sub-list?
    Tabulate four times and add a number for an ordered list or a -, *, or a + for an unordered list.
    4. An ordered list?
    Use numbers followed by a period.
    5. A pre-formatted block?
    Use 3 backticks to open and 3 backticks to close.
    6. Make something bold?
    Put two astericks before and after a word or phrase.
    7. Make something italic?
    Put one asterick before and after a word or phrase.
    8. Make something both bold and italic?
    Put three astericks before and after a word or phrase.
    9. Link to something?
    Put a link text in brackets followed by the URL in parentheses.
    10. Add a horizontal rule (bar or separator)?
    Put three or more astericks, dashes, or underscores on one line.
6. Besides VSCode, how can you view a rendered Markdown file?
You can view a rendered Markdown file by using any text editor software.

(Discussion 15m)

## Tips, Questions, and Insights

Use this section to reflect on and summarize what you learned in this activity.

We learned how Markdown can be very useful for stylizing and typing information.