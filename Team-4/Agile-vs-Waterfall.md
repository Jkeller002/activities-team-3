# Agile vs Waterfall


## Roles

Assign each role below to a team member. Put your recorder in the middle.

* Manager: RS
* Recorder: LG
* Spokesperson: BL
* Quality Control: BL


## Model 1

Time: 20m

See Sweeney's article "Agile vs Waterfall: Which Method is More Successful" https://clearcode.cc/blog/agile-vs-waterfall-method/

Use the above article to answer the following questions.

1. In `Agile vs Waterfall: Project Success and Failure Rates`...

    a. Results from what studies are reported?
    The studies the results are reported from are Ambysoft's 2013 Project Success Rates Survey and 2015 CHAOS report from the Standish Group.

    b. When were the studies conducted?
    The Ambysoft study was conducted in 2013 while the CHAOS report was conducted in 2015.

    c. Summarize the results of the studies.
    The Ambysoft study concluded that the agile method has a 64% success rate while the waterfall model has a 49% success rate. The CHAOS report found that the agile method produces a higher success rate than the waterfall method.

    d. What conclusions do you draw from these results?
    The agile method is the ideal and more efficient method of software development than the waterfall method.


2. In `1. The Software-Development Process`, inspect the diagrams for
   the Agile and Waterfall processes.

    a. How are Agile and Waterfall the same?
    Agile and Waterfall are the same through the fact that they both utilize conducting development through series of stages/phases.

    b. How are Agile and Waterfall different?
    Agile and Waterfall are different is Agile allows developers to come across and resolve obstacles sooner than Waterfall because Agile allows developers to test each part of the application sooner and decide how the rest of the application's development should progress.


3. In `2. Application Testing` ...

    a. Summarize the differences in testing between Agile and Waterfall.


4. In `3. Value delivery` ...

    a. Explain the "Boat" diagram.


    b. The Mona Lisa is often used to illustrate incremental and iterative development. Search for these and summarize your understanding of these concepts and how they relate to agile development. How does this modify your understanding of Agile and Waterfall?

5. If you have more time, see if there are any more recent studies with new data comparing Agile and Waterfall. Link to them here.

(Class discussion 15m)


## Other articles on Waterfall vs Agile

*   [https://clearcode.cc/blog/agile-vs-waterfall-method/](https://clearcode.cc/blog/agile-vs-waterfall-method/)
*   [https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/](https://vitalitychicago.com/blog/agile-projects-are-more-successful-traditional-projects/)
*   [https://www.infoq.com/articles/standish-chaos-2015](https://www.infoq.com/articles/standish-chaos-2015)


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
