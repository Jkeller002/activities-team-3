# Copyright and Licensing - Activity

## Roles

Assign the following roles to your team members. Remember to rotate roles
daily so that everyone has a chance at every roll equally often.

* Manager & Reporter: BL
* Recorder & Pilot: LG
* Quality Control: RS
* Efficiency Expert: RS

## Objectives

*   Explain when a work becomes copyrighted and who owns the copyright.
*   Identify what you can and cannot do with unlicensed software.
*   Explain the role of licenses.
*   Explain the importance of licenses to open-source software.
*   Identify the license of published software.
*   Articulate why you would or would not be comfortable submitting code to a project with a particular license.

## Model

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

## Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea?
No, you cannot copyright the idea.

2. You have written a new program based on that idea, is that software copyright-able?
Yes, the software is copyright-able.

3. You have written a new program, what do you have to do to copyright that program?
The copyright is applied automically to the creator of the program. You would need to get the program registered in order for the copyright to be enforced.

4. If you own the copyrights to something, what can you do that no one else can do? (see <http://www.bitlaw.com/copyright/scope.html> )
You are able to have the right to reproduce the copyrighted work, the right to prepare derivative works based on the work, the right to distribute copies of the work to the public, the right to perform the copyrighted work publicly, and the right to display the copyrighted work publicly.

5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?
You can do anything, but it is risky due to potential legal issues. Since copyright is applied automatically, a copyright still does exist. Without a license, you cannot do anything with the project except view the program.

6. What is the purpose of a license?
The purpose of a license is to grant users the rights to use, to modify, and to share your software.

7. When you license your program do you give up your copyright?
Your copyright is not given up if your program is licensed.

8. As the copyright holder, once you license your program, could you later license it under a different license?
You can later license your program under a different license.

(Class discussion 10m)

## Questions - Part 2

Time: 10m

9. If you relicense your program, can someone still use your program under the previous license?
Yes, someone can still use your program under the previous license.

10. Can you sell open-source licensed software?
Yes, you can sell open-source licensed software.

11. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?
The company would own the copyright to the code that you right. The license would be whichever license the company selects.

12. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?
The person that initially created the program owns the copyright. Under the MIT license, you must share the copyright with your friends. 

13. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?
You must allow other people to user, change, share, and collaborate with your software to suit their individual needs. The person that owns the copyright on what they write is the owner of the software. The license the code is licensed under is the GPL-v3.0 license.

14. Try to identify the license for the following projects:
    1. <https://github.com/openmrs/openmrs-core>
    Mozilla Public Licensem version 2.0
    2. <https://github.com/apache/fineract>
    Apache License version 2.0
    3. Find VSCodium on GitHub.
    MIT License

15. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.
Mozilla Public License version 2.0
Cans: Commerical Use, Modify, Distribute, Sublicense, Place Warranty, Use Patent Claims
Cannots: Use Trademark, Hold Liable
Musts: Include Copyright, Include License, Disclose Source, Include Original

Apache License verison 2.0
Cans: Commercial Use, Modify, Distribute, Sublicense, Place Warranty, Private Use, Use Patent Claims
Cannots: Hold Liable, Use Trademark
Musts: Include Copyright, Include License, State Changes, Include Notice

MIT License
Cans: Commercial Use, Modify, Distribute, Sublicense, Private Use
Cannots: Hold Liable,
Musts: Include Copyright, Include License

16. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?
A permissive open-source license allows developers to make things proprietary and requires a warranty disclaimer while a non-permissive open-source license guarantees user freedoms and prevents developers from restricting or locking-in users; moreover, it prevents the building of proprietary software.

(Class discussion 10m)

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
