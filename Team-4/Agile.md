# Agile

## Roles

* Manager: RS
* Recorder: LG
* Spokesperson: BL
* Researcher: BL


## Model 1 - What is Agile?

12m Video: *What is Agile?* by Mark Shead. May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

Questions: 5m

1. Is Agile a process?
No, Agile is not a process.

2. Is Agile a framework?
No, Agile is not a framework.

3. Is Agile a set of tools?
No, Agile is not a set of tools.

4. What is Agile?
Agile is a set of values and principles.

5. What does Agile help companies and developers do?
Agile helps companies and developers work collaboratively, make necessary changes based on customer needs, and make better decisions.

Class discussion: 5m


## Model 2 - The Agile Manifesto

Time: 10m

At the core of The Agile Manifesto are four value statements.
They are structured as follows:

```
We value

    Customer  collaboration  over contract  negotiation.

    Individuals  and interactions  over  processes  and tools

    Working  software  over  comprehensive documentation

    Reponding  to change  over  following  a plan

```
The missing phrases are mixed up below. Unscramble them to reconstruct the
value statements of the Agile Manifesto. Try to do it without looking up the Manifesto.

* `Individuals`
* `Working`
* `Responding`
* `Customer`
* `documentation`
* `and tools`
* `negotiation`
* `collaboration`
* `following`
* `and interactions`
* `software`
* `comprehensive`
* `proccess`
* `to change`
* `a plan`
* `contract`

Once you are satisfied, check your work against [The Agile Manifesto](https://www.agilealliance.org/agile101/the-agile-manifesto/) . How many statements did your team get right (ignoring the order of the statements)?
4/4

1. Does the Manifesto imply that projects should not use tools or follow a process?
The Manifesto implies that projects should not use tools or follow a process.

2. Does the Manifesto imply that projects should not write any documentation?
The Manifesto implies that projects should not write any documentation.

3. Does the Manifesto imply that projects should not have contracts with clients?
The Manifesto implies that projects should not have contracts with clients.

4. Does the Manifesto imply that projects should not create or follow a plan?
The Manifesto implies that projects should no create or follow a plan.

5. Based on their values, why would an Agile team reject the waterfall method to developing software?
An Agile team would reject the Waterfall method to developing software because the Agile method puts on features and products quicker and allows for faster change than the Waterfall method.

Class discussion: 10m


## Model 3

Time: 20m

Below are the 12 Agile principles that embody the 4 Agile value statements.
For each, identify which value statement the principle supports or is most related to,
and be prepared to explain how.

1. Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
"Customer collaboration over contract negotiation" because communicating with the customer allows the developers to create a better final product.

2. Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
"Responding to change over following a plan" because addressing change can create happy customers instead of just sticking to a strict plan.

3. Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
"Working software over comprehensive documentation" because pushing out software more frequently instead of writing documentation allows customers to enjoy new features more frequently as well as to check in with the customer to see if the customer is satisifed.

4. Business people and developers must work together daily throughout the project.
"Individuals and interactions over processes and tools" because working collaboratively allows for better and bigger ideas.

5. Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
"Individuals and interactions over processes and tools" because communication with individuals can help the developers fix any issues and create new ideas.

6. The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
"Individuals and interactions over processes and tools" because communication with your team allows everyone to be on the same page and to get more work done.

7. Working software is the primary measure of progress.
"Working software over comprehensive documentation" because it is important to have your software working properly over writing documentation.

8. Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
"Individuals and interactions over processes and tools" because it is important that development goes smoothly and there is no slow down in development.

9. Continuous attention to technical excellence and good design enhances agility.
"Working software over comprehensive documentation" because the continous attention from the software by the users would allow developers to push out new updates and fixes quicker.

10. Simplicity--the art of maximizing the amount of work not done--is essential.
"Customer collaboration over contract negotiation" because the customer is able to tell the developers what should be fixed and what is working well.

11. The best architectures, requirements, and designs emerge from self-organizing teams.
"Individuals and interactions over processes and tools" because working collabortively and the communication between the teams allows them to remain organized and stay on the same page on issues and ideas.

12. At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.
"Responding to change over following a plan" because changing due to any issues or new ideas will allow a team to be successful and make adjustments accordingly in a quick and efficient manner.

Class discussion: 20m


## References

* 12m Video: *What is Agile?* by Mark Sheed May 31, 2016. <https://youtu.be/Z9QbYZh1YXY>

* The Agile Manifesto

  * Values

    * Original: <https://agilemanifesto.org/>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/the-agile-manifesto/>

  * Principles

    * Original: <https://agilemanifesto.org/principles.html>

    * AgileAlliance.org: <https://www.agilealliance.org/agile101/12-principles-behind-the-agile-manifesto/>


---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br>
Copyright 2020, Stoney Jackson &lt;dr.stoney@gmail.com&gt;
